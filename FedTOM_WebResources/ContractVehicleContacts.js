﻿(function (document, angular) {
    "use strict";
    var app = angular.module('MyApp', []);//ngInputModified
    console.log("Into Contract Vehicle Contacts");
    app.controller('ContractVehicleContacts', ContractVehicleContacts);
    function ContractVehicleContacts($scope) {
        try {
            var Xrm = parent.Xrm;
            var CVguid = Xrm.Page.data.entity.getId();
            var CVID = CVguid.substring(1, 37);
            console.log("CVID:- " + CVID);
            GetContractVehicleContacts();
            $scope.ContractVehicleContactList = [];
            $scope.PartnerContactList = [];
            $scope.allPartnersSelected = false;
            $scope.allContactsSelected = false;
            function GetContractVehicleContacts() {
                $scope.ContractVehicleContactList = [];
                $scope.EditArray = [];
                $scope.allContactsSelected = false;
                $scope.allPartnersSelected = false;
                $scope.savebtnCssClass = "slds-hide";
                $scope.SaveBtnCount = 0;
                try {
                    
                    var qry = "?$select=fedcap_name,_fedcap_accountname_value,_fedcap_contactname_value,fedcap_email,fedcap_mainpoc,fedcap_receivesmassemails&$filter=_fedcap_contractvehiclename_value eq " + CVID + "";
                    Xrm.WebApi.retrieveMultipleRecords("fedcap_contractvehiclecontact", qry)
                       .then(
                       function success(responseCVC) {
                           $scope.ContractVehicleContactList = [];
                           for (var i = 0; i < responseCVC.entities.length; i++) {
                               
                               var accid = responseCVC.entities[i]["_fedcap_accountname_value"];
                               var acc = responseCVC.entities[i]["_fedcap_accountname_value@OData.Community.Display.V1.FormattedValue"];
                               if (accid === null) {
                                   accid = "";
                                   acc = "";
                               }
                               var contactid = responseCVC.entities[i]["_fedcap_contactname_value"];
                               var contactname = responseCVC.entities[i]["_fedcap_contactname_value@OData.Community.Display.V1.FormattedValue"];
                               debugger;
                               if (contactid === null) {
                                   contactid = "";
                                   contactname = "";
                               }
                               $scope.ContractVehicleContactList.push(
                                   {
                                       "index": i,
                                       "editclass": "slds-show",
                                       "undoclass": "slds-hide",
                                       "addclass": "slds-hide",
                                       "cvcid": responseCVC.entities[i]["fedcap_contractvehiclecontactid"],
                                       "accountid": accid,
                                       "accountname": acc,
                                       "contactid": contactid,
                                       "contactname": contactname,
                                       "email": responseCVC.entities[i]["fedcap_email"],
                                       "fedcap_mainpoc": responseCVC.entities[i]["fedcap_mainpoc"],
                                       "mainpoc": responseCVC.entities[i]["fedcap_mainpoc"],
                                       "fedcap_receivesmassemails": responseCVC.entities[i]["fedcap_receivesmassemails"],
                                       "receivesmassemails": responseCVC.entities[i]["fedcap_receivesmassemails"],
                                   });
                           }
                           $scope.$apply();
                       },
                       function fail(error) {
                           var data = error;
                       }
                       );

                } catch (e) {
                }
            }
            $scope.GetPartnerContacts = function ()
                {
                $scope.btnDisable = true;
                debugger;
                var accountId = "";
                var fetchXML = "";
                fetchXML = '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">'
                fetchXML += '<entity name="contact"><attribute name="fullname" />'
                fetchXML += '<attribute name="contactid" />'
                fetchXML += '<attribute name="emailaddress1" />'
                fetchXML += '<attribute name="parentcustomerid" />'
                fetchXML += '<order attribute="fullname" descending="false" />'
                fetchXML += '<filter type="and">'
                fetchXML += '<condition attribute="parentcustomerid" operator="not-null" />'
                fetchXML += '<condition attribute="parentcustomerid" operator="in">'
                for (var i = 0; i < $scope.VehiclePartnersList.length; i++) {
                    if ($scope.VehiclePartnersList[i].isChecked) {
                        //uiname = "' + $scope.VehiclePartnersList[i].accountname + '"
                        fetchXML += '<value  uitype="account">{' + $scope.VehiclePartnersList[i].accountid + '}</value>'
                    }
                }
                fetchXML += '</condition></filter></entity></fetch>'
                
                var encodedFetchXml = encodeURI(fetchXML);
                debugger;
                $scope.allContactsSelected = false;
                $scope.PartnerContactList = [];
                try {
                    //var qry = "?$select=fedcap_name,_fedcap_accountname_value,_fedcap_contactname_value,fedcap_email,fedcap_mainpoc,fedcap_receivesmassemails&$filter=_fedcap_contractvehiclename_value eq " + CVID + "";
                    debugger;
                    //Xrm.WebApi.retrieveMultipleRecords("fedcap_contractvehiclecontact", qry)
                    Xrm.WebApi.retrieveMultipleRecords("contact", "?fetchXml=" + encodedFetchXml)
                       .then(
                       function success(responseCVC) {
                           $scope.btnDisable = false;
                           debugger;
                           for (var i = 0; i < responseCVC.entities.length; i++) {

                               var filteredData = $scope.ContractVehicleContactList.filter(
                                        function (response) {
                                            debugger;
                                            var contactid1 = response["contactid"];
                                            var contactid2 = responseCVC.entities[i]["contactid"]
                                            return contactid1 === responseCVC.entities[i]["contactid"];
                                        });
                               if (filteredData.length === 0)
                               {
                                   var accid = responseCVC.entities[i]["_parentcustomerid_value"];
                                   var acc = responseCVC.entities[i]["_parentcustomerid_value@OData.Community.Display.V1.FormattedValue"];
                                   if (accid === null) {
                                       accid = "";
                                       acc = "";
                                   }
                                   var contactid = responseCVC.entities[i]["contactid"];
                                   var contactname = responseCVC.entities[i]["fullname"];
                                   if (contactid === null) {
                                       contactid = "";
                                       contactname = "";
                                   }
                                   $scope.PartnerContactList.push(
                                       {
                                           "index": i,
                                           "editclass": "slds-show",
                                           "undoclass": "slds-hide",
                                           "addclass": "slds-hide",
                                           "accountid": accid,
                                           "accountname": acc,
                                           "contactid": contactid,
                                           "contactname": contactname,
                                           "email": responseCVC.entities[i]["emailaddress1"],
                                           "mailpoc": false,
                                           "receivesmassemail": false,
                                       });
                               }
                           }
                           $scope.$apply();
                       },
                       function fail(error) {
                           $scope.btnDisable = false;
                           var data = error;
                       }
                       );

                } catch (e) {
                    $scope.btnDisable = false;
                }
            }
            $scope.GetVehiclePartners = function () {
                $scope.btnDisable = false;
                //var CVguid = Xrm.Page.getAttribute('fedcap_contractvehiclename').getValue()[0].id;
                //var CVID = CVguid.substring(1, 37);
                $("#txtPartnerSearch").val("");
                $("#txtContactSearch").val("");
                $scope.VehiclePartnersList = [];
                $scope.PartnerContactList = [];
                $scope.allContactsSelected = false;
                $scope.allPartnersSelected = false;
                try {
                    var qry = "?$select=new_name,_fedcap_accountforvehiclepartner_value,new_tm_toma__role__c,_new_tm_toma__contract_vehicle__c_value&$filter=_new_tm_toma__contract_vehicle__c_value eq " + CVID + "";
                    Xrm.WebApi.retrieveMultipleRecords("new_vehiclepartners", qry)
                       .then(
                       function success(responseCV) {
                           var VPCount = 0;
                           for (var i = 0; i < responseCV.entities.length; i++) {
                               var roleid = responseCV.entities[i]["new_tm_toma__role__c"];
                               var role = responseCV.entities[i]["new_tm_toma__role__c@OData.Community.Display.V1.FormattedValue"];
                               if (roleid === null) {
                                   roleid = "";
                                   role = "";
                               }
                               var accid = responseCV.entities[i]["_fedcap_accountforvehiclepartner_value"];
                               var acc = responseCV.entities[i]["_fedcap_accountforvehiclepartner_value@OData.Community.Display.V1.FormattedValue"];
                               if (accid === null) {
                                   accid = "";
                                   acc = "";
                               }
                               $scope.VehiclePartnersList.push(
                                   {
                                       "index": VPCount,
                                       "vehiclepartnersid": responseCV.entities[i]["new_vehiclepartnersid"],
                                       "accountid": accid,
                                       "accountname": acc,
                                       "roleid": roleid,
                                       "role": role,
                                   });
                               VPCount = VPCount + 1;

                           }
                           $scope.$apply();
                       },
                       function fail(error) {
                           var data = error;
                       }
                       );

                } catch (e) {
                }
            }
            $scope.savebtnCssClass = "slds-hide";
            $scope.EditArray = [];
            $scope.Edit = function (cvc) {
                try {
                    $scope.savebtnCssClass = "slds-show";
                    cvc.editclass = 'slds-hide';
                    cvc.undoclass = 'slds-show';
                    $scope.EditArray.push(
                        {
                            "index": cvc.index,
                            "editclass": cvc.editclass,
                            "undoclass": cvc.undoclass,
                            "addclass": cvc.addclass,
                            "cvcid": cvc.cvcid,
                            "accountid": cvc.accountid,
                            "accountname": cvc.accountname,
                            "contactid": cvc.contactid,
                            "contactname": cvc.contactname,
                            "email": cvc.email,
                            "mainpoc": cvc.mainpoc,
                            "receivesmassemails": cvc.receivesmassemails,
                        });
                    $scope.SaveBtnCount = $scope.SaveBtnCount + 1;

                } catch (e) {
                }
                //$scope.ContractVehicleContactList.forEach(function (v) {
                //    if (v.index === index) {
                //    }
                //});
            }
            $scope.Undo = function (cvc) {
                debugger;
                try {
                    cvc.editclass = 'slds-show';
                    cvc.undoclass = 'slds-hide';
                    if ($scope.SaveBtnCount !== 0) {
                        $scope.SaveBtnCount = $scope.SaveBtnCount - 1;
                    }
                    if ($scope.SaveBtnCount === 0) {
                        $scope.savebtnCssClass = "slds-hide";
                    }
                    var index = $scope.EditArray.indexOf(cvc);
                    cvc.receivesmassemails = cvc.fedcap_receivesmassemails;
                    cvc.mainpoc = cvc.fedcap_mainpoc;
                    $scope.EditArray.splice(index, 1);
                } catch (e) {

                }
            }
            $scope.Delete = function (cvc) {
                try {
                    var dialogLabelAndText = { confirmButtonLabel: "Yes", cancelButtonLabel: "No", text: "Are you sure you want to delete this Contract Vehicle Contact record? This action will delete all Teaming Partner records on related Task Orders.", title: "Confirmation" };
                    var dialogOptions = { height: 200, width: 450 };
                    Xrm.Navigation.openConfirmDialog(dialogLabelAndText, dialogOptions).then(
                      function (success) {
                          if (success.confirmed) {
                              // todo code for deletion
                              if ($scope.SaveBtnCount !== 0) {
                                  $scope.SaveBtnCount = $scope.SaveBtnCount - 1;
                              }
                              if ($scope.SaveBtnCount === 0) {
                                  $scope.savebtnCssClass = "slds-hide";
                              }
                              debugger;
                              Xrm.WebApi.deleteRecord("fedcap_contractvehiclecontact", cvc.cvcid).then(
                                                  function success(result) {
                                                      debugger;
                                                      //var index = $scope.ContractVehicleContactList.indexOf(cvc);
                                                      //$scope.ContractVehicleContactList.splice(index, 1);
                                                      GetContractVehicleContacts();
                                                      Xrm.Navigation.openAlertDialog({ confirmButtonLabel: "Yes", text: "Record Deleted!" });
                                                  },
                                                  function (error) {
                                                      debugger;
                                                  }
                                              );
                              //var index = $scope.ContractVehicleContactList.indexOf(cvc);
                              //$scope.ContractVehicleContactList.splice(index, 1);
                          }
                      });
                } catch (e) {
                    debugger;

                }
            }
            $scope.SaveBtnCount = 0;
            $scope.SaveRecords = function () {
                try {
                    $scope.SaveBtnCount = 0;
                    $scope.savebtnCssClass = "slds-hide";
                    debugger;
                    for (var i = 0; i < $scope.EditArray.length; i++) {
                        var objArry = {};
                        var filteredData = $scope.ContractVehicleContactList.filter(
                                       function (response) {
                                           var cvc_id = response["cvcid"];
                                           return cvc_id === $scope.EditArray[i]["cvcid"];
                                       });

                        if (filteredData !== 0) {
                            objArry.fedcap_mainpoc = filteredData[0].mainpoc;
                            objArry.fedcap_receivesmassemails = filteredData[0].receivesmassemails;
                            Xrm.WebApi.updateRecord("fedcap_contractvehiclecontact", filteredData[0].cvcid, objArry).then(
                                                function success(result) {
                                                    debugger;
                                                    var editLen = $scope.EditArray.length;
                                                    if (i === editLen) {
                                                        GetContractVehicleContacts();
                                                    }
                                                },
                                                function (error) {
                                                    debugger;
                                                }
                                            );
                        }
                        
                    }

                } catch (e) {
                    debugger;
                }
            }
            $scope.btnDisable = false;
            $scope.AddContacts = function () {
                debugger;
                try {
                    $scope.btnDisable = true;
                    var arr = [];
                    var saveCount = 0;
                    for (var i = 0; i < $scope.PartnerContactList.length; i++) {
                        if ($scope.PartnerContactList[i].isChecked) {
                            var receivesmassemail = $scope.PartnerContactList[i].receivesmassemail;
                            arr.push(
                                {
                                    "contactid": $scope.PartnerContactList[i].contactid,
                                    "accountid": $scope.PartnerContactList[i].accountid,
                                    "email": $scope.PartnerContactList[i].email,
                                    "mailpoc": $scope.PartnerContactList[i].fedcap_mainpoc,
                                    "receivesmassemail": $scope.PartnerContactList[i].fedcap_receivesmassemails,
                                }
                                )
                        }
                    }
                    if (arr.length === 0) {
                        $scope.btnDisable = false;
                        return;
                    }
                    for (var i = 0; i < arr.length; i++) {
                        var objArry = {};
                        objArry["fedcap_AccountName@odata.bind"] = "/accounts(" + arr[i].accountid + ")";
                        objArry["fedcap_ContactName@odata.bind"] = "/contacts(" + arr[i].contactid + ")";
                        objArry["fedcap_ContractVehicleName@odata.bind"] = "/fedcap_contractvehiclecontacts(" + CVID + ")"; 
                        objArry.fedcap_email = arr[i].email;
                        objArry.fedcap_mainpoc = arr[i].mailpoc;
                        objArry.fedcap_receivesmassemails = arr[i].receivesmassemail;
                        debugger;
                        Xrm.WebApi.createRecord("fedcap_contractvehiclecontact", objArry).then(
                                            function success(result) {
                                                debugger;
                                                saveCount = saveCount + 1;
                                                if (saveCount === arr.length) {
                                                    $scope.btnDisable = false;
                                                    GetContractVehicleContacts();
                                                    $('#myModal').modal('hide');
                                                }
                                            },
                                            function (error) {
                                                $scope.btnDisable = false;
                                                debugger;
                                            }
                                        );
                    }
                } catch (e) {
                    $scope.btnDisable = false;
                }
            }
            $scope.Refresh = function () {
                try {
                    GetContractVehicleContacts();
                } catch (e) {

                }
            }
            $scope.OpenAccount = function (cvc) {
                try {
                    var parameters = {};
                    debugger;
                    var windowOptions = {
                        openInNewWindow: false
                    };
                    Xrm.Utility.openEntityForm("account", cvc.accountid, parameters, windowOptions);
                } catch (e) {

                }
            }
            $scope.OpenContact = function (cvc) {
                try {
                    var parameters = {};
                    debugger;
                    var windowOptions = {
                        openInNewWindow: false
                    };
                    Xrm.Utility.openEntityForm("contact", cvc.contactid, parameters, windowOptions);
                } catch (e) {

                }
            }

            // This executes when entity in table is checked
            $scope.selectPartner = function () {
                // If any entity is not checked, then uncheck the "allItemsSelected" checkbox
                for (var i = 0; i < $scope.VehiclePartnersList.length; i++) {
                    if (!$scope.VehiclePartnersList[i].isChecked) {
                        $scope.allPartnersSelected = false;
                        return;
                    }
                }

                //If not the check the "allItemsSelected" checkbox
                $scope.allPartnersSelected = true;
            };

            // This executes when checkbox in table header is checked
            $scope.selectAllPartner = function () {
                // Loop through all the entities and set their isChecked property
                for (var i = 0; i < $scope.VehiclePartnersList.length; i++) {
                    $scope.VehiclePartnersList[i].isChecked = $scope.allPartnersSelected;
                }
            };

            // This executes when entity in table is checked
            $scope.selectContact = function () {
                // If any entity is not checked, then uncheck the "allItemsSelected" checkbox
                for (var i = 0; i < $scope.PartnerContactList.length; i++) {
                    if (!$scope.PartnerContactList[i].isChecked) {
                        $scope.allContactsSelected = false;
                        return;
                    }
                }

                //If not the check the "allItemsSelected" checkbox
                $scope.allContactsSelected = true;
            };

            // This executes when checkbox in table header is checked
            $scope.selectAllContact = function () {
                // Loop through all the entities and set their isChecked property
                for (var i = 0; i < $scope.PartnerContactList.length; i++) {
                    $scope.PartnerContactList[i].isChecked = $scope.allContactsSelected;
                }
            };
        } catch (e) {
        }
    }
})(document, angular);