﻿(function (document, angular) {
    "use strict";
    var app = angular.module('MyApp', []);//ngInputModified
    console.log("Into Task Order Contacts");
    app.controller('TaskOrderContacts', TaskOrderContacts);
    function TaskOrderContacts($scope) {
        try {
            var Xrm = parent.Xrm;
            var TOguid = Xrm.Page.data.entity.getId();
            var TOID = TOguid.substring(1, 37);
            console.log("TOID:- " + TOID);
            GetTaskOrderContacts();
            $scope.TaskOrderContactList = [];
            $scope.PartnerContactList = [];
            $scope.allPartnersSelected = false;
            $scope.allContactsSelected = false;
            function GetTaskOrderContacts() {
                //$("#btnAddTOContact").attr("disabled", false);
                $scope.btnDisable = false;
                //$("#btnShowContacts").attr("disabled", false);
                $scope.TaskOrderContactList = [];
                $scope.EditArray = [];
                $scope.savebtnCssClass = "slds-hide";
                $scope.allContactsSelected = false;
                $scope.allPartnersSelected = false;
                $scope.SaveBtnCount = 0;
                try {

                    var qry = "?$select=fedcap_taskordercontact,_fedcap_accounts_value,_fedcap_taskorder_contact_value,fedcap_email,fedcap_mainpoc,fedcap_receivesmassemails&$filter=_fedcap_taskorder_value eq " + TOID + "";
                    Xrm.WebApi.retrieveMultipleRecords("fedcap_taskordercontacts", qry)
                       .then(
                       function success(responseCVC) {
                           $scope.TaskOrderContactList = [];
                           for (var i = 0; i < responseCVC.entities.length; i++) {

                               var accid = responseCVC.entities[i]["_fedcap_accounts_value"];
                               var acc = responseCVC.entities[i]["_fedcap_accounts_value@OData.Community.Display.V1.FormattedValue"];
                               if (accid === null) {
                                   accid = "";
                                   acc = "";
                               }
                               var contactid = responseCVC.entities[i]["_fedcap_taskorder_contact_value"];
                               var contactname = responseCVC.entities[i]["_fedcap_taskorder_contact_value@OData.Community.Display.V1.FormattedValue"];
                               debugger;
                               if (contactid === null) {
                                   contactid = "";
                                   contactname = "";
                               }
                               $scope.TaskOrderContactList.push(
                                   {
                                       "index": i,
                                       "editclass": "slds-show",
                                       "undoclass": "slds-hide",
                                       "addclass": "slds-hide",
                                       "tocid": responseCVC.entities[i]["fedcap_taskordercontactsid"],
                                       "accountid": accid,
                                       "accountname": acc,
                                       "contactid": contactid,
                                       "contactname": contactname,
                                       "email": responseCVC.entities[i]["fedcap_email"],
                                       "fedcap_mainpoc": responseCVC.entities[i]["fedcap_mainpoc"],
                                       "mainpoc": responseCVC.entities[i]["fedcap_mainpoc"],
                                       "fedcap_receivesmassemails": responseCVC.entities[i]["fedcap_receivesmassemails"],
                                       "receivesmassemails": responseCVC.entities[i]["fedcap_receivesmassemails"],
                                   });
                           }
                           $scope.$apply();
                       },
                       function fail(error) {
                           var data = error;
                       }
                       );

                } catch (e) {
                }
            }
            $scope.GetTeamingContacts = function () {
                //$("#btnShowContacts").attr("disabled", true);
                $scope.btnDisable = true;
                debugger;
                var accountId = "";
                var fetchXML = "";
                fetchXML = '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">'
                fetchXML += '<entity name="contact"><attribute name="fullname" />'
                fetchXML += '<attribute name="contactid" />'
                fetchXML += '<attribute name="emailaddress1" />'
                fetchXML += '<attribute name="parentcustomerid" />'
                fetchXML += '<order attribute="fullname" descending="false" />'
                fetchXML += '<filter type="and">'
                fetchXML += '<condition attribute="parentcustomerid" operator="not-null" />'
                fetchXML += '<condition attribute="parentcustomerid" operator="in">'
                for (var i = 0; i < $scope.TaskOrderPartnersList.length; i++) {
                    if ($scope.TaskOrderPartnersList[i].isChecked) {
                        //uiname = "' + $scope.TaskOrderPartnersList[i].accountname + '"
                        fetchXML += '<value  uitype="account">{' + $scope.TaskOrderPartnersList[i].accountid + '}</value>'
                    }
                }
                fetchXML += '</condition></filter></entity></fetch>'

                var encodedFetchXml = encodeURI(fetchXML);
                debugger;
                $scope.allContactsSelected = false;
                $scope.PartnerContactList = [];
                try {
                    debugger;
                    Xrm.WebApi.retrieveMultipleRecords("contact", "?fetchXml=" + encodedFetchXml)
                       .then(
                       function success(responseCVC) {
                           debugger;
                           //$("#btnShowContacts").attr("disabled", false);
                           $scope.btnDisable = false;

                           for (var i = 0; i < responseCVC.entities.length; i++) {

                               var filteredData = $scope.TaskOrderContactList.filter(
                                        function (response) {
                                            debugger;
                                            var contactid1 = response["contactid"];
                                            var contactid2 = responseCVC.entities[i]["contactid"]
                                            return contactid1 === responseCVC.entities[i]["contactid"];
                                        });
                               if (filteredData.length === 0) {
                                   var accid = responseCVC.entities[i]["_parentcustomerid_value"];
                                   var acc = responseCVC.entities[i]["_parentcustomerid_value@OData.Community.Display.V1.FormattedValue"];
                                   if (accid === null) {
                                       accid = "";
                                       acc = "";
                                   }
                                   var contactid = responseCVC.entities[i]["contactid"];
                                   var contactname = responseCVC.entities[i]["fullname"];
                                   if (contactid === null) {
                                       contactid = "";
                                       contactname = "";
                                   }
                                   $scope.PartnerContactList.push(
                                       {
                                           "index": i,
                                           "editclass": "slds-show",
                                           "undoclass": "slds-hide",
                                           "addclass": "slds-hide",
                                           "accountid": accid,
                                           "accountname": acc,
                                           "contactid": contactid,
                                           "contactname": contactname,
                                           "email": responseCVC.entities[i]["emailaddress1"],
                                           "mailpoc": false,
                                           "receivesmassemail": false,
                                       });
                               }
                           }
                           $scope.$apply();
                       },
                       function fail(error) {
                           var data = error;
                            $scope.btnDisable = false;
                           //$("#btnShowContacts").attr("disabled", false);
                       }
                       );

                } catch (e) {
                    $scope.btnDisable = false;
                    //$("#btnShowContacts").attr("disabled", false);
                }
            }
            $scope.GetTaskOrderPartners = function () {
                debugger;
                $scope.btnDisable = false;
                //var CVguid = Xrm.Page.getAttribute('fedcap_contractvehiclename').getValue()[0].id;
                //var TOID = CVguid.substring(1, 37);
                $("#txtPartnerSearch").val("");
                $("#txtContactSearch").val("");
                $scope.TaskOrderPartnersList = [];
                $scope.PartnerContactList = [];
                $scope.allContactsSelected = false;
                $scope.allPartnersSelected = false;
                debugger;
                try {
                    var qry = "?$select=fedcap_opportunityteamname,fedcap_partner,fedcap_partnerrole,_fedcap_taskorder_value,_fedcap_vehiclepartner_value&$filter=_fedcap_taskorder_value eq " + TOID + "";
                    Xrm.WebApi.retrieveMultipleRecords("fedcap_teamingpartners", qry)
                       .then(
                       function success(responseCV) {
                           var VPCount = 0;
                           debugger;
                           for (var i = 0; i < responseCV.entities.length; i++) {
                               var roleid = responseCV.entities[i]["fedcap_partnerrole"];
                               var role = responseCV.entities[i]["fedcap_partnerrole"];
                               if (roleid === null) {
                                   roleid = "";
                                   role = "";
                               }
                               var accid = responseCV.entities[i]["_fedcap_vehiclepartner_value"];//vehiclepartnerlookupid
                               var vehiclepartnerlookupid = responseCV.entities[i]["_fedcap_vehiclepartner_value"];//vehiclepartnerlookupid
                               var acc = responseCV.entities[i]["fedcap_partner"];
                               if (accid === null) {
                                   accid = "";
                                   acc = "";
                               }
                               $scope.TaskOrderPartnersList.push(
                                   {
                                       "index": VPCount,
                                       "teamingpartnersid": responseCV.entities[i]["fedcap_teamingpartnersid"],
                                       "accountid": accid,
                                       "vehiclepartnerlookupid": vehiclepartnerlookupid,
                                       "accountname": acc,
                                       "roleid": roleid,
                                       "role": role,
                                   });
                               VPCount = VPCount + 1;
                               if (VPCount === responseCV.entities.length) {
                                   debugger;
                                   angular.forEach($scope.TaskOrderPartnersList, function (value, key) {
                                       //myAccess.push(value.name);
                                       debugger;
                                       Xrm.WebApi.retrieveRecord("new_vehiclepartners", value.vehiclepartnerlookupid)
                                        .then(
                                           function succ(response) {
                                               debugger;
                                               console.log(response["_fedcap_accountforvehiclepartner_value"]);
                                               value.accountid = response["_fedcap_accountforvehiclepartner_value"];
                                           },
                                       function failed(error) {
                                           debugger;
                                           var data = error;
                                       }
                                        );
                                   });
                                   debugger;
                                   //for (var i = 0; i < TaskOrderPartnersList.length; i++) {
                                       
                                   //}
                               }

                           }
                           $scope.$apply();
                       },
                       function fail(error) {
                           debugger;
                           var data = error;
                       }
                       );

                } catch (e) {
                    debugger;
                }
                debugger;
                $scope.$apply();
            }
            $scope.savebtnCssClass = "slds-hide";
            $scope.EditArray = [];
            $scope.Edit = function (cvc) {
                try {
                    $scope.savebtnCssClass = "slds-show";
                    cvc.editclass = 'slds-hide';
                    cvc.undoclass = 'slds-show';
                    $scope.EditArray.push(
                        {
                            "index": cvc.index,
                            "editclass": cvc.editclass,
                            "undoclass": cvc.undoclass,
                            "addclass": cvc.addclass,
                            "tocid": cvc.tocid,
                            "accountid": cvc.accountid,
                            "accountname": cvc.accountname,
                            "contactid": cvc.contactid,
                            "contactname": cvc.contactname,
                            "email": cvc.email,
                            "mainpoc": cvc.mainpoc,
                            "receivesmassemails": cvc.receivesmassemails,
                        });
                    $scope.SaveBtnCount = $scope.SaveBtnCount + 1;

                } catch (e) {
                }
                //$scope.TaskOrderContactList.forEach(function (v) {
                //    if (v.index === index) {
                //    }
                //});
            }
            $scope.Undo = function (cvc) {
                debugger;
                try {
                    cvc.editclass = 'slds-show';
                    cvc.undoclass = 'slds-hide';
                    if ($scope.SaveBtnCount !== 0) {
                        $scope.SaveBtnCount = $scope.SaveBtnCount - 1;
                    }
                    if ($scope.SaveBtnCount === 0) {
                        $scope.savebtnCssClass = "slds-hide";
                    }
                    var index = $scope.EditArray.indexOf(cvc);
                    cvc.receivesmassemails = cvc.fedcap_receivesmassemails;
                    cvc.mainpoc = cvc.fedcap_mainpoc;
                    $scope.EditArray.splice(index, 1);
                } catch (e) {

                }
            }
            $scope.Delete = function (cvc) {
                try {
                    debugger;
                    //var dialogLabelAndText = { confirmButtonLabel: "Proceed", cancelButtonLabel: "Clear & Exit", text: "This is a Confirmation Dialog", title: "Dialog" };
                    //var dialogOptions = { height: 200, width: 450 };
                    //Xrm.Navigation.openConfirmDialog(dialogLabelAndText, dialogOptions).then(
                    //  function (success) {
                    //      if (success.confirmed)
                    //          Xrm.Navigation.openAlertDialog({ confirmButtonLabel: "Yes", text: "Process completed!" });
                    //      else
                    //          Xrm.Navigation.openAlertDialog({ confirmButtonLabel: "Yes", text: "Exit and Clear Process!" });
                    //  });
                    var dialogLabelAndText = { confirmButtonLabel: "Yes", cancelButtonLabel: "No", text: "Are you sure you want to delete this Task Order Contact record?", title: "Confirmation" };
                    var dialogOptions = { height: 100, width: 450 };
                    Xrm.Navigation.openConfirmDialog(dialogLabelAndText, dialogOptions).then(
                      function (success) {
                          if (success.confirmed) {
                              // todo code for deletion
                              if ($scope.SaveBtnCount !== 0) {
                                  $scope.SaveBtnCount = $scope.SaveBtnCount - 1;
                              }
                              if ($scope.SaveBtnCount === 0) {
                                  $scope.savebtnCssClass = "slds-hide";
                              }
                              debugger;
                              Xrm.WebApi.deleteRecord("fedcap_taskordercontacts", cvc.tocid).then(
                                                  function success(result) {
                                                      debugger;
                                                      //var index = $scope.TaskOrderContactList.indexOf(cvc);//
                                                      //$scope.TaskOrderContactList.splice(index, 1);
                                                      GetTaskOrderContacts();
                                                      Xrm.Navigation.openAlertDialog({ confirmButtonLabel: "Yes", text: "Record deleted!", title: "Success" });

                                                  },
                                                  function (error) {
                                                      debugger;
                                                  }
                                              );
                              //var index = $scope.TaskOrderContactList.indexOf(cvc);
                              //$scope.TaskOrderContactList.splice(index, 1);
                          }
                      });
                    //if (confirm("Are you sure you want to delete this Task Order Contact record?")) {
                      
                    //}
                } catch (e) {
                    debugger;

                }
            }
            $scope.SaveBtnCount = 0;
            $scope.SaveRecords = function () {
                try {
                    $scope.SaveBtnCount = 0;
                    $scope.savebtnCssClass = "slds-hide";
                    debugger;
                    for (var i = 0; i < $scope.EditArray.length; i++) {
                        var objArry = {};
                        var filteredData = $scope.TaskOrderContactList.filter(
                                       function (response) {
                                           var cvc_id = response["tocid"];
                                           return cvc_id === $scope.EditArray[i]["tocid"];
                                       });

                        if (filteredData !== 0) {
                            objArry.fedcap_mainpoc = filteredData[0].mainpoc;
                            objArry.fedcap_receivesmassemails = filteredData[0].receivesmassemails;
                            Xrm.WebApi.updateRecord("fedcap_taskordercontacts", filteredData[0].tocid, objArry).then(
                                                function success(result) {
                                                    debugger;
                                                    var editLen = $scope.EditArray.length;
                                                    if (i === editLen) {
                                                        GetTaskOrderContacts();
                                                    }
                                                },
                                                function (error) {
                                                    debugger;
                                                }
                                            );
                        }

                    }

                } catch (e) {
                    debugger;
                }
            }
            $scope.btnDisable = false;
            $scope.AddContacts = function () {
                debugger;
                //$("#btnAddTOContact").attr("disabled", true);
                $scope.btnDisable = true;
                try {
                    var arr = [];
                    var saveCount = 0;
                    for (var i = 0; i < $scope.PartnerContactList.length; i++) {
                        if ($scope.PartnerContactList[i].isChecked) {
                            var receivesmassemail = $scope.PartnerContactList[i].receivesmassemail;
                            arr.push(
                                {
                                    "contactid": $scope.PartnerContactList[i].contactid,
                                    "accountid": $scope.PartnerContactList[i].accountid,
                                    "email": $scope.PartnerContactList[i].email,
                                    "mailpoc": $scope.PartnerContactList[i].fedcap_mainpoc,
                                    "receivesmassemail": $scope.PartnerContactList[i].fedcap_receivesmassemails,
                                }
                                )
                        }
                    }
                    if (arr.length === 0) {
                        $scope.btnDisable = false;
                        return;
                    }
                    for (var i = 0; i < arr.length; i++) {
                        var objArry = {};
                        objArry["fedcap_Accounts@odata.bind"] = "/accounts(" + arr[i].accountid + ")";
                        objArry["fedcap_TaskOrder_Contact@odata.bind"] = "/contacts(" + arr[i].contactid + ")";
                        objArry["fedcap_TaskOrder@odata.bind"] = "/fedcap_taskorders(" + TOID + ")";
                        objArry.fedcap_email = arr[i].email;
                        objArry.fedcap_mainpoc = arr[i].mailpoc;
                        objArry.fedcap_receivesmassemails = arr[i].receivesmassemail;
                        debugger;
                        Xrm.WebApi.createRecord("fedcap_taskordercontacts", objArry).then(
                                            function success(result) {
                                                debugger;
                                                saveCount = saveCount + 1;
                                                if (saveCount === arr.length) {
                                                    //$("#btnAddTOContact").attr("disabled", false);
                                                    $scope.btnDisable = false;
                                                    GetTaskOrderContacts();
                                                    $('#myModal').modal('hide');
                                                }
                                            },
                                            function (error) {
                                                debugger;
                                                $scope.btnDisable = false;
                                            }
                                        );
                    }
                } catch (e) {
                    $scope.btnDisable = false;
                }
            }
            $scope.Refresh = function () {
                try {
                    GetTaskOrderContacts();
                } catch (e) {

                }
            }
            $scope.OpenAccount = function (cvc) {
                try {
                    var parameters = {};
                    debugger;
                    var windowOptions = {
                        openInNewWindow: false
                    };
                    Xrm.Utility.openEntityForm("account", cvc.accountid, parameters, windowOptions);
                } catch (e) {

                }
            }
            $scope.OpenContact = function (cvc) {
                try {
                    var parameters = {};
                    debugger;
                    var windowOptions = {
                        openInNewWindow: false
                    };
                    Xrm.Utility.openEntityForm("contact", cvc.contactid, parameters, windowOptions);
                } catch (e) {

                }
            }

            // This executes when entity in table is checked
            $scope.selectPartner = function () {
                // If any entity is not checked, then uncheck the "allItemsSelected" checkbox
                for (var i = 0; i < $scope.TaskOrderPartnersList.length; i++) {
                    if (!$scope.TaskOrderPartnersList[i].isChecked) {
                        $scope.allPartnersSelected = false;
                        return;
                    }
                }

                //If not the check the "allItemsSelected" checkbox
                $scope.allPartnersSelected = true;
            };

            // This executes when checkbox in table header is checked
            $scope.selectAllPartner = function () {
                // Loop through all the entities and set their isChecked property
                for (var i = 0; i < $scope.TaskOrderPartnersList.length; i++) {
                    $scope.TaskOrderPartnersList[i].isChecked = $scope.allPartnersSelected;
                }
            };

            // This executes when entity in table is checked
            $scope.selectContact = function () {
                // If any entity is not checked, then uncheck the "allItemsSelected" checkbox
                for (var i = 0; i < $scope.PartnerContactList.length; i++) {
                    if (!$scope.PartnerContactList[i].isChecked) {
                        $scope.allContactsSelected = false;
                        return;
                    }
                }

                //If not the check the "allItemsSelected" checkbox
                $scope.allContactsSelected = true;
            };

            // This executes when checkbox in table header is checked
            $scope.selectAllContact = function () {
                // Loop through all the entities and set their isChecked property
                for (var i = 0; i < $scope.PartnerContactList.length; i++) {
                    $scope.PartnerContactList[i].isChecked = $scope.allContactsSelected;
                }
            };
        } catch (e) {
        }
    }
})(document, angular);