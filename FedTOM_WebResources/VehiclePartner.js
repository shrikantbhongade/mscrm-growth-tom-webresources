﻿(function (document, angular) {
    "use strict";
    var app = angular.module('MyApp', []);//ngInputModified
    console.log("Into Vehicle Partner");
    app.controller('VehiclePartner', VehiclePartner);
    function VehiclePartner($scope) {
        try {
            var Xrm = parent.Xrm;
            var CVguid = Xrm.Page.data.entity.getId();
            var CVID = CVguid.substring(1, 37);
            console.log("CVID:- " + CVID);
            GetVehiclePartners();
            $scope.VehiclePartnersList = [];
            $scope.rolelist = [{
                id: 100000001,
                value: "Prime Contractor"
            },
            {
                id: 100000000,
                value: "Sub-Contractor"
            }
            ];
            function GetVehiclePartners() {
                debugger;
                $scope.VehiclePartnersList = [];
                $scope.EditArray = [];
                $scope.savebtnCssClass = "slds-hide";
                $scope.SaveBtnCount = 0;

                try {
                    var qry = "?$select=new_name,_fedcap_accountforvehiclepartner_value,new_tm_toma__role__c,_new_tm_toma__contract_vehicle__c_value&$filter=_new_tm_toma__contract_vehicle__c_value eq " + CVID + "";
                    debugger;
                    Xrm.WebApi.retrieveMultipleRecords("new_vehiclepartners", qry)
                       .then(
                       function success(responseCV) {
                           debugger;
                           $scope.VehiclePartnersList = [];
                           //var data = responseCV;
                           //$scope.VehiclePartnersList = responseCV.entities;
                           for (var i = 0; i < responseCV.entities.length; i++) {
                               if (responseCV.entities[i]["new_tm_toma__role__c"] === null) {

                               }
                               var roleid = responseCV.entities[i]["new_tm_toma__role__c"];
                               var role = responseCV.entities[i]["new_tm_toma__role__c@OData.Community.Display.V1.FormattedValue"];
                               if (roleid === null) {
                                   roleid = "";
                                   role = "";
                               }
                               var accid = responseCV.entities[i]["_fedcap_accountforvehiclepartner_value"];
                               var acc = responseCV.entities[i]["_fedcap_accountforvehiclepartner_value@OData.Community.Display.V1.FormattedValue"];
                               if (accid === null) {
                                   accid = "";
                                   acc = "";
                               }
                               $scope.VehiclePartnersList.push(
                                   {
                                       "index": i,
                                       "editclass": "slds-show",
                                       "undoclass": "slds-hide",
                                       "addclass": "slds-hide",
                                       "vehiclepartnersid": responseCV.entities[i]["new_vehiclepartnersid"],
                                       "accountid": accid,
                                       "accountname": acc,
                                       "roleid": roleid,
                                       "_roleid": roleid,
                                       "role": role,
                                   });
                           }
                           $scope.$apply();
                       },
                       function fail(error) {
                           var data = error;
                       }
                       );
                       
                } catch (e) {
                }
            }
            $scope.savebtnCssClass = "slds-hide";
            $scope.EditArray = [];
            $scope.Edit = function (vp) {
                try {
                    $scope.savebtnCssClass = "slds-show";
                    vp.editclass = 'slds-hide';
                    vp.undoclass = 'slds-show';
                    $scope.EditArray.push(
                        {
                            "index": vp.index,
                            "editclass": vp.editclass,
                            "undoclass": vp.undoclass,
                            "addclass": vp.addclass,
                            "vehiclepartnersid": vp.vehiclepartnersid,
                            "accountid": vp.accountid,
                            "accountname": vp.accountname,
                            "roleid": vp.roleid,
                            "_roleid": vp._roleid,
                            "role": vp.role,
                        });
                    $scope.SaveBtnCount = $scope.SaveBtnCount + 1;

                } catch (e) {
                }
                //$scope.VehiclePartnersList.forEach(function (v) {
                //    if (v.index === index) {
                //    }
                //});
            }
            $scope.Undo = function (vp) {
                debugger;
                try {
                    vp.editclass = 'slds-show';
                    vp.undoclass = 'slds-hide';
                    if ($scope.SaveBtnCount !== 0) {
                        $scope.SaveBtnCount = $scope.SaveBtnCount - 1;
                    }
                    if ($scope.SaveBtnCount === 0) {
                        $scope.savebtnCssClass = "slds-hide";
                    }
                    var index = $scope.EditArray.indexOf(vp);
                    vp.roleid = vp._roleid;
                    $scope.EditArray.splice(index, 1);
                } catch (e) {

                }
            }
            $scope.Delete = function (vp) {
                try {
                    var dialogLabelAndText = { confirmButtonLabel: "Yes", cancelButtonLabel: "No", text: "Are you sure you want to delete this Vehicle Partner record? This action will delete all Teaming Partner records on related Task Orders.", title: "Confirmation" };
                    var dialogOptions = { height: 200, width: 450 };
                    Xrm.Navigation.openConfirmDialog(dialogLabelAndText, dialogOptions).then(
                      function (success) {
                          if (success.confirmed) {
                              // todo code for deletion
                              if ($scope.SaveBtnCount !== 0) {
                                  $scope.SaveBtnCount = $scope.SaveBtnCount - 1;
                              }
                              if ($scope.SaveBtnCount === 0) {
                                  $scope.savebtnCssClass = "slds-hide";
                              }
                              if (vp.vehiclepartnersid === "") {
                                  var index = $scope.VehiclePartnersList.indexOf(vp);
                                  $scope.VehiclePartnersList.splice(index, 1);
                                  $scope.EditArray.splice(index, 1);
                              }
                              else {
                                  Xrm.WebApi.deleteRecord("new_vehiclepartners", vp.vehiclepartnersid).then(
                                                  function success(result) {
                                                      //var index = $scope.VehiclePartnersList.indexOf(vp);
                                                      //$scope.VehiclePartnersList.splice(index, 1);
                                                      GetVehiclePartners();
                                                      Xrm.Navigation.openAlertDialog({ confirmButtonLabel: "Yes", text: "Record deleted!" });
                                                  },
                                                  function (error) {
                                                  }
                                              );
                                  //var index = $scope.VehiclePartnersList.indexOf(vp);
                                  //$scope.VehiclePartnersList.splice(index, 1);
                              }
                          }
                          $scope.$apply();
                      });
                } catch (e) {
                }
            }
            $scope.SaveBtnCount = 0;
            $scope.SaveRecords = function () {
                try {
                    debugger;
                    $scope.SaveBtnCount = 0;
                    $scope.savebtnCssClass = "slds-hide";
                    for (var i = 0; i < $scope.EditArray.length; i++) {
                        var objArry = {};
                        debugger;
                        var filteredData = $scope.VehiclePartnersList.filter(
                                                               function (response) {
                                                                   var index = response["index"];
                                                                   return index === $scope.EditArray[i]["index"];
                                                               });
                        if (filteredData.length !== 0) {
                            if (filteredData[0].vehiclepartnersid === "") {
                                objArry.new_tm_toma__role__c = filteredData[0].roleid;
                                objArry["fedcap_AccountForVehiclepartner@odata.bind"] = "/accounts(" + filteredData[0].accountid + ")";
                                objArry["new_TM_TOMA__Contract_Vehicle__c@odata.bind"] = "/new_vehiclepartnerses(" + CVID + ")";
                                //new_contractvehicles
                                Xrm.WebApi.createRecord("new_vehiclepartners", objArry).then(
                                                    function success(result) {
                                                        debugger;
                                                        if (i === $scope.EditArray.length) {
                                                            GetVehiclePartners();
                                                            $scope.savebtnCssClass = "slds-hide";
                                                        }
                                                    },
                                                    function (error) {
                                                        debugger;
                                                    }
                                                );
                            }
                            else {
                                objArry.new_tm_toma__role__c = filteredData[0].roleid;
                                Xrm.WebApi.updateRecord("new_vehiclepartners", filteredData[0].vehiclepartnersid, objArry).then(
                                                    function success(result) {
                                                        debugger;
                                                        if (i === $scope.EditArray.length) {
                                                            GetVehiclePartners();
                                                            $scope.savebtnCssClass = "slds-hide";
                                                        }
                                                    },
                                                    function (error) {
                                                        debugger;
                                                    }
                                                );
                            }
                        }
                    }
                } catch (e) {
                    debugger;
                }
            }
            $scope.AddRow = function () {
                debugger;
                try {
                    var index = $scope.VehiclePartnersList.length + 1;
                    $scope.VehiclePartnersList.push(
                                   {
                                       "index": index,
                                       "editclass": "slds-hide",
                                       "undoclass": "slds-hide",
                                       "addclass": "slds-show",
                                       "vehiclepartnersid": "",
                                       "accountid": "",
                                       "accountname": "",
                                       "roleid": 100000001,
                                       "role": "",
                                   });
                    $scope.EditArray.push(
                                            {
                                                "index": index,
                                                "editclass": "slds-hide",
                                                "undoclass": "slds-hide",
                                                "addclass": "slds-show",
                                                "vehiclepartnersid": "",
                                                "accountid": "",
                                                "accountname": "",
                                                "roleid": 100000001,
                                                "role": "",
                                            });

                    $scope.savebtnCssClass = "slds-show";
                    $scope.SaveBtnCount = $scope.SaveBtnCount + 1;
                    $scope.$apply();

                    //var parameters = {};
                    //debugger;
                    //var new_name = Xrm.Page.getAttribute("new_name").getValue();
                    //parameters["new_tm_toma__contract_vehicle__c"] = CVID;
                    //parameters["new_tm_toma__contract_vehicle__cname"] = Xrm.Page.getAttribute("new_name").getValue();
                    ////parameters["formid"] = 'FEAFC18E-1E79-4385-8BF5-A662B24FF476';
                    //var windowOptions = {
                    //    openInNewWindow: true
                    //};
                    //Xrm.Utility.openEntityForm("new_vehiclepartners", null, parameters, windowOptions);

                } catch (e) {

                }
            }
            var row = null;
            $scope.OpenLookup = function (vp, entityName) {
                debugger;
                //controlId = cntrlId + "_id";
                //control_Id = cntrlId;
                row = vp;
                var lookupObjectParams = {};
                lookupObjectParams.entityTypes = [entityName];
                lookupObjectParams.defaultEntityType = entityName;
                lookupObjectParams.allowMultiSelect = false;
                Xrm.Utility.lookupObjects(lookupObjectParams).then(CallbackFunctionLookup, LookupCancelCallback)
            }
            function CallbackFunctionLookup(selectedItems) {
                if (selectedItems != null && selectedItems.length > 0) {
                    var lookup = [{ id: selectedItems[0].id, typename: selectedItems[0].typename, name: selectedItems[0].name }];
                    //$("#" + control_Id).val(lookup[0].name);
                    //$("#" + controlId).val(lookup[0].id.substring(1, 37));
                    row.accountname = lookup[0].name;
                    row.accountid = lookup[0].id.substring(1, 37);
                    $scope.$apply();
                }
            }
            function LookupCancelCallback() { }
            $scope.Refresh = function () {
                try {
                    GetVehiclePartners();
                } catch (e) {

                }
            }
            $scope.OpenAccount = function (cv) {
                try {
                    var parameters = {};
                    debugger;
                    var windowOptions = {
                        openInNewWindow: false
                    };
                    Xrm.Utility.openEntityForm("account", cv.accountid, parameters, windowOptions);
                } catch (e) {

                }
            }
        } catch (e) {
        }
    }
})(document, angular);