"use strict";
var XrmObj = parent.Xrm;
var XTOName = "";
var xOpptyFound;

function Call_TaskOrderToCreateOppty_Workflow() {
    "use strict";
    try {
        var ErrorMsg = "";
        var OpptyFound;
        var TOName = XrmObj.Page.getAttribute("fedcap_taskordertitle").getValue();
        var qry = "?$select=name&$filter=contains(name,('" + TOName + "'))";
        XrmObj.WebApi.retrieveMultipleRecords("opportunity", qry).then(
        function success(result) {
            if (result.entities.length === 0) {
                var serverURL = XrmObj.Page.context.getClientUrl();
                var fedcap_taskorderid = XrmObj.Page.data.entity.getId();
                fedcap_taskorderid = fedcap_taskorderid.substring(1, 37);
                var workflowId = "{c1e68a5e-b262-4a52-8f96-b2099a4990ef}";
                //Execute Workflow
                executeWorkflow(fedcap_taskorderid, workflowId, serverURL);
            }
            else {
                XrmObj.Page.ui.setFormNotification("Task Order " + TOName + " is already exist in Opportunity.", "WARNING", "TETS");
                setTimeout(function () { XrmObj.Page.ui.clearFormNotification(ErrorMsg); }, 5000);
            }
        },
        function (error) {
            XrmObj.Page.ui.setFormNotification(error.message, "WARNING", "TETS");
            setTimeout(function () { XrmObj.Page.ui.clearFormNotification(ErrorMsg); }, 5000);
        });

    } catch (e) {
        XrmObj.Page.ui.setFormNotification(e.message.toString(), "WARNING", "TETS");
        setTimeout(function () { XrmObj.Page.ui.clearFormNotification(ErrorMsg); }, 5000);
    }
}

function retrieveMultipleAccounts(TOName) {

}


function retrieveMultipleAccounts1() {
    "use strict";
    XTOName = XrmObj.Page.getAttribute("fedcap_taskordertitle").getValue();
    var OpptyFound;
    var qry = "?$select=name&$filter=contains(name,'(" + XTOName + "')";
    XrmObj.WebApi.retrieveMultipleRecords("opportunity", qry).then(
    function success(result) {
        for (var i = 0; i < result.entities.length; i++) {
            var Name = result.entities[i].name;
            if (Name === XTOName) {
                xOpptyFound = true;
                return;

            }
            else {
                xOpptyFound = false;
            }
        }
    },
    function (error) {
    });
}


function executeWorkflow(fedcap_taskorderid, workflowId, serverURL) {
    "use strict";
    //CheckDuplicateTaskorderInOppty();
    var ErrorMsg = "";
    var functionName = "executeWorkflow >>";
    var query = "";
    try {
        //Define the query to execute the action
        query = "workflows(" + workflowId.replace("}", "").replace("{", "") + ")/Microsoft.Dynamics.CRM.ExecuteWorkflow";
        var data = {
            "EntityId": fedcap_taskorderid
        };
        //Create request
        // request url
        //https://org.crm.dynamics.com/api/data/v8.2/workflows(“f0ca33cc-23fd-496f-80e1-693873a951ca”)/Microsoft.Dynamics.CRM.ExecuteWorkflow
        var req = new XMLHttpRequest();
        req.open("POST", serverURL + "/api/data/v8.2/" + query, true);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");

        req.onreadystatechange = function () {

            if (this.readyState === 4 /* complete */) {
                req.onreadystatechange = null;
                if (this.status === 200) {
                    //success callback this returns null since no return value available.
                    var result = JSON.parse(this.response);
                    ErrorMsg = result;
                    XrmObj.Page.ui.setFormNotification("Opportunity is Created!!", "WARNING", ErrorMsg);
                    setTimeout(function () { XrmObj.Page.ui.clearFormNotification(ErrorMsg); }, 5000);

                } else {
                    //error callback
                    var error = JSON.parse(this.response).error;
                    ErrorMsg = error;
                    XrmObj.Page.ui.setFormNotification(error.message, "ERROR", ErrorMsg);
                    setTimeout(function () { XrmObj.Page.ui.clearFormNotification(ErrorMsg); }, 5000);
                }
            }
        };
        req.send(JSON.stringify(data));
    } catch (e) {
        throwError(functionName, e);
    }
}

function hideContols() {
    //var div = parent.document.getElementById("dataSetRoot_TeamingPartner");
    //div.style.display = "none";

}

function OpenPopUp() {
    //window.location("https://fedcap.crm.dynamics.com//WebResources/fedcap_TeamingPartner.html");

}


