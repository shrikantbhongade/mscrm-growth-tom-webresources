(function (document, angular) {
    "use strict";
    var app = angular.module('MyApp', []);//ngInputModified
    console.log("Into TOMA_QuestionnaireCRUD");
    app.controller('Questionnaires', Questionnaires);
    function Questionnaires($scope) {
        try {
            var Xrm = parent.Xrm;
            var TOMQguid = Xrm.Page.data.entity.getId();
            var TOMQID = TOMQguid.substring(1, 37);
            console.log("TOMQID:- " + TOMQID);
            $scope.QuestionList = [];
            $scope.TemplateList = [];
            $scope.TOMAEmailTemplateList = [];
            $scope.SurveyName = "";
            $scope.Description = "";
            $scope.IsActive = false;
            $scope.dvView = false;
            if (TOMQID === "") {
                $scope.dvView = true;
            }
            $scope.EditView = function () {
                try {
                    $scope.dvView = true;
                } catch (e) {

                }
            }
            GetQuestionnaires();
            GetTOMAEmailTemplate();
            $scope.GetTemplates = function ()
            {
                $("#txtTemplateSearch").val("");
                $scope.TemplateList = [];
                try {
                    //var qry = "?$filter=_fedtom_survey_value eq " + TOMQID + "";
                    Xrm.WebApi.retrieveMultipleRecords("template")
                     .then(
                        function succ(response) {
                            for (var i = 0; i < response.entities.length; i++) {
                                $scope.TemplateList.push(
                                    {
                                        "index":i + 1,
                                        "templateid": response.entities[i]["templateid"],
                                        "title": response.entities[i]["title"],
                                        "description": response.entities[i]["description"]
                                    }
                                    );
                            }
                            $scope.$apply();
                        },
                    function failed(error) {
                        var data = error;
                    }
                     );
                } catch (e) {

                }
            }
            function GetTOMAEmailTemplate() {
                try {
                    $scope.TOMAEmailTemplateList = [];
                    var qry = "?$filter=_fedtom_teamingrequestquestionnaire_value eq " + TOMQID + "";
                    Xrm.WebApi.retrieveMultipleRecords("fedtom_toma_questionaries_email_template",qry)
                     .then(
                        function succ(response) {
                            for (var i = 0; i < response.entities.length; i++) {
                                $scope.TOMAEmailTemplateList.push(
                                    {
                                        "index": i + 1,
                                        "guid": response.entities[i]["fedtom_toma_questionaries_email_templateid"],
                                        "templateid": response.entities[i]["fedtom_toma_email_templateid"],
                                        "title": response.entities[i]["fedtom_toma_email_template"],
                                        "description": response.entities[i]["fedtom_description"]
                                    }
                                    );
                            }
                            $scope.$apply();
                        },
                    function failed(error) {
                        var data = error;
                    }
                     );
                } catch (e) {

                }
            }
            function GetQuestionnaires() {
                try {
                    if (TOMQID === "") {
                        $scope.QuestionList.push(
                                       {
                                           "index": 1, "collapsecls": "panel-collapse collapse in",
                                           "questionname": "Question", "questiontext": "",
                                           "option1": "Option 1", "option2": "Option 2",
                                           "option3": "Option 3", "option4": "Option 4",
                                           "option5": "Option 5", "option6": "Option 6",
                                           "option7": "Option 7", "option8": "Option 8",
                                           "option9": "Option 9", "option10": "Option 10",
                                           "option1show": true, "option2show": true,
                                           "option3show": false, "option4show": false,
                                           "option5show": false, "option6show": false,
                                           "option7show": false, "option8show": false,
                                           "option9show": false, "option10show": false,
                                           "option1txt": "", "option2txt": "",
                                           "option3txt": "", "option4txt": "",
                                           "option5txt": "", "option6txt": "",
                                           "option7txt": "", "option8txt": "",
                                           "option9txt": "", "option10txt": "",
                                           "answers": "", "descriptiveanswer": "",
                                           "ismandatory": false, "questiontype": "0",
                                           "surveyid": "", "survey": "",
                                           "sequence": "", "queid": "",
                                           "delete": false, "addoption": true,"questiontypename":""
                                       });
                        $scope.$apply();

                        //$scope.QuestionList.push(
                        //               {
                        //                   "index": 2, "collapsecls": "panel-collapse collapse",
                        //                   "questionname": "Question", "questiontext": "Question",
                        //                   "option1": "Option 1", "option2": "Option 2",
                        //                   "option3": "Option 3", "option4": "Option 4",
                        //                   "option5": "Option 5", "option6": "Option 6",
                        //                   "option7": "Option 7", "option8": "Option 8",
                        //                   "option9": "Option 9", "option10": "Option 10",
                        //                   "option1show": true, "option2show": true,
                        //                   "option3show": false, "option4show": false,
                        //                   "option5show": false, "option6show": false,
                        //                   "option7show": false, "option8show": false,
                        //                   "option9show": false, "option10show": false,
                        //                   "option1txt": "", "option2txt": "",
                        //                   "option3txt": "", "option4txt": "",
                        //                   "option5txt": "", "option6txt": "",
                        //                   "option7txt": "", "option8txt": "",
                        //                   "option9txt": "", "option10txt": "",
                        //                   "answers": "", "descriptiveanswer": "",
                        //                   "ismandatory": false, "questiontype": "0",
                        //                   "surveyid": "", "survey": "",
                        //                   "sequence": "", "queid": "",
                        //                   "delete": true
                        //               });
                    }
                    else {
                        Xrm.WebApi.retrieveRecord("fedtom_tomaquestionnaire", TOMQID)
                                          .then(
                                             function succTOMA(responseTOMA) {
                                                 $scope.SurveyName = responseTOMA["fedtom_name"];
                                                 $scope.Description = responseTOMA["fedtom_description"];
                                                 $scope.IsActive = responseTOMA["fedtom_isactive"];
                                                 //var qry = "?$select=fedtom_tomaquestionsid,fedtom_questiontype,fedtom_question,fedtom_option1,fedtom_option2,fedtom_option3,fedtom_option4,fedtom_option5,fedtom_option6,fedtom_option7,fedtom_option8,fedtom_option9,fedtom_option10,fedtom_answer,fedtom_descriptiveanswer,fedtom_ismandatory,_fedtom_survey_value,fedtom_sequence&$filter=_fedtom_survey_value eq " + TOMQID + "";
                                                 var qry = "?$orderby=fedtom_sequence asc&$filter=_fedtom_survey_value eq " + TOMQID + "";
                                                 Xrm.WebApi.retrieveMultipleRecords("fedtom_tomaquestions", qry)
                                                  .then(
                                                     function succ(response) {
                                                         for (var i = 0; i < response.entities.length; i++)
                                                         {
                                                             var btnDelete = true;
                                                             var btnAddOption = true;
                                                             var index = i + 1;
                                                             if (i === 0) {
                                                                 btnDelete = false;
                                                             }
                                                             if (response.entities[i]["fedtom_questiontype"].toString() === "3" || response.entities[i]["fedtom_questiontype"].toString() === 3) {
                                                                 btnAddOption = false;
                                                             }
                                                             if (response.entities[i]["fedtom_questiontype"].toString() === "4" || response.entities[i]["fedtom_questiontype"].toString() === 4) {
                                                                 btnAddOption = false;
                                                             }
                                                             
                                                             $scope.QuestionList.push(
                                                                       {
                                                                           "index": index, "collapsecls": "panel-collapse collapse",
                                                                           "questionname": "Question", "questiontext": response.entities[i]["fedtom_question"],
                                                                           "option1": "Option 1", "option2": "Option 2",
                                                                           "option3": "Option 3", "option4": "Option 4",
                                                                           "option5": "Option 5", "option6": "Option 6",
                                                                           "option7": "Option 7", "option8": "Option 8",
                                                                           "option9": "Option 9", "option10": "Option 10",
                                                                           "option1show": response.entities[i]["fedtom_showoption1"], "option2show": response.entities[i]["fedtom_showoption2"],
                                                                           "option3show": response.entities[i]["fedtom_showoption3"], "option4show": response.entities[i]["fedtom_showoption4"],
                                                                           "option5show": response.entities[i]["fedtom_showoption5"], "option6show": response.entities[i]["fedtom_showoption6"],
                                                                           "option7show": response.entities[i]["fedtom_showoption7"], "option8show": response.entities[i]["fedtom_showoption8"],
                                                                           "option9show": response.entities[i]["fedtom_showoption9"], "option10show": response.entities[i]["fedtom_showoption10"],
                                                                           "option1txt": response.entities[i]["fedtom_option1"], "option2txt": response.entities[i]["fedtom_option2"],
                                                                           "option3txt": response.entities[i]["fedtom_option3"], "option4txt": response.entities[i]["fedtom_option4"],
                                                                           "option5txt": response.entities[i]["fedtom_option5"], "option6txt": response.entities[i]["fedtom_option6"],
                                                                           "option7txt": response.entities[i]["fedtom_option7"], "option8txt": response.entities[i]["fedtom_option8"],
                                                                           "option9txt": response.entities[i]["fedtom_option9"], "option10txt": response.entities[i]["fedtom_option10"],
                                                                           "answers": response.entities[i]["fedtom_answer"], "descriptiveanswer": response.entities[i]["fedtom_descriptiveanswer"],
                                                                           "ismandatory": response.entities[i]["fedtom_ismandatory"], "questiontype": response.entities[i]["fedtom_questiontype"].toString(),
                                                                           "surveyid": response.entities[i]["_fedtom_survey_value"], "survey": response.entities[i]["_fedtom_survey_value@OData.Community.Display.V1.FormattedValue"],
                                                                           "sequence": response.entities[i]["fedtom_sequence"], "queid": response.entities[i]["fedtom_tomaquestionsid"],
                                                                           "delete": btnDelete, "addoption": btnAddOption, "questiontypename": response.entities[i]["fedtom_questiontype@OData.Community.Display.V1.FormattedValue"].toString()
                                                                       });
                                                         }
                                                         $scope.$apply();
                                                     },
                                                 function failed(error) {
                                                     var data = error;
                                                 }
                                                  );
                                             },
                                         function failedTOMA(errorTOMA) {
                                             var data = errorTOMA;
                                         }
                                          );
                    }
                } catch (e) {

                }
            }
            $scope.SaveRecords = function () {
                try {
                    var isProceed = true;// for saving in progress
                    for (var i = 0; i < $scope.QuestionList.length; i++) {
                        if ($scope.QuestionList[i].questiontext === "") {
                            isProceed = false;
                            var message = { confirmButtonLabel: "Close", text: "Please enter question for " + (i + 1) + " index." };
                            var alertOptions = { height: 150, width: 280 };
                            Xrm.Navigation.openAlertDialog(message, alertOptions).then(
                                function success(result) {
                                },
                                function (error) {
                                }
                            );
                            break;
                        }
                        if ($scope.QuestionList[i].sequence === "") {
                            isProceed = false;
                            var message = { confirmButtonLabel: "Close", text: "Please enter question sequence for " + (i + 1) + " index." };
                            var alertOptions = { height: 150, width: 280 };
                            Xrm.Navigation.openAlertDialog(message, alertOptions).then(
                                function success(result) {
                                },
                                function (error) {
                                }
                            );
                            break;
                        }
                        else {
                            debugger;
                            var secCount = 0;
                            for (var j = 0; j < $scope.QuestionList.length; j++) {
                                if ($scope.QuestionList[i].sequence === $scope.QuestionList[j].sequence) {
                                    secCount = secCount + 1;
                                }
                            }
                            if (secCount > 1) {
                                isProceed = false;
                                var message = { confirmButtonLabel: "Close", text: "Duplicate sequence no found. Please correct." };
                                var alertOptions = { height: 150, width: 280 };
                                Xrm.Navigation.openAlertDialog(message, alertOptions).then(
                                    function success(result) {
                                    },
                                    function (error) {
                                    }
                                );
                                break;
                            }
                        }
                    }
                    if (isProceed) {
                        var objArry = {};
                        objArry["fedtom_name"] = $scope.SurveyName;
                        objArry["fedtom_description"] = $scope.Description;
                        objArry["fedtom_isactive"] = $scope.IsActive;
                        if (TOMQID === "") {
                            if ($scope.SurveyName === "") {
                                Xrm.Page.ui.setFormNotification("Survey Name name is required.", "WARNING", "1")
                                setTimeout(function () {
                                    Xrm.Page.ui.clearFormNotification("1");
                                }, 3000);
                                return;
                            }
                            Xrm.WebApi.createRecord("fedtom_tomaquestionnaire", objArry).then(
                                            function successTOMA(resultTOMA) {
                                                var recordId = resultTOMA.id;
                                                for (var i = 0; i < $scope.QuestionList.length; i++) {
                                                    var objArryQue = {};
                                                    objArryQue["fedtom_Survey@odata.bind"] = "/fedtom_tomaquestionnaires(" + resultTOMA.id + ")";
                                                    //objArryQue["fedtom_name"] = $scope.QuestionList[i].index;
                                                    objArryQue["fedtom_option1"] = $scope.QuestionList[i].option1txt;
                                                    objArryQue["fedtom_option2"] = $scope.QuestionList[i].option2txt;
                                                    objArryQue["fedtom_option3"] = $scope.QuestionList[i].option3txt;
                                                    objArryQue["fedtom_option4"] = $scope.QuestionList[i].option4txt;
                                                    objArryQue["fedtom_option5"] = $scope.QuestionList[i].option5txt;
                                                    objArryQue["fedtom_option6"] = $scope.QuestionList[i].option6txt;
                                                    objArryQue["fedtom_option7"] = $scope.QuestionList[i].option7txt;
                                                    objArryQue["fedtom_option8"] = $scope.QuestionList[i].option8txt;
                                                    objArryQue["fedtom_option9"] = $scope.QuestionList[i].option9txt;
                                                    objArryQue["fedtom_option10"] = $scope.QuestionList[i].option10txt;

                                                    objArryQue["fedtom_showoption1"] = $scope.QuestionList[i].option1show;
                                                    objArryQue["fedtom_showoption2"] = $scope.QuestionList[i].option2show;
                                                    objArryQue["fedtom_showoption3"] = $scope.QuestionList[i].option3show;
                                                    objArryQue["fedtom_showoption4"] = $scope.QuestionList[i].option4show;
                                                    objArryQue["fedtom_showoption5"] = $scope.QuestionList[i].option5show;
                                                    objArryQue["fedtom_showoption6"] = $scope.QuestionList[i].option6show;
                                                    objArryQue["fedtom_showoption7"] = $scope.QuestionList[i].option7show;
                                                    objArryQue["fedtom_showoption8"] = $scope.QuestionList[i].option8show;
                                                    objArryQue["fedtom_showoption9"] = $scope.QuestionList[i].option9show;
                                                    objArryQue["fedtom_showoption10"] = $scope.QuestionList[i].option10show;

                                                    objArryQue["fedtom_answer"] = $scope.QuestionList[i].answers;
                                                    objArryQue["fedtom_descriptiveanswer"] = $scope.QuestionList[i].descriptiveanswer;
                                                    objArryQue["fedtom_ismandatory"] = $scope.QuestionList[i].ismandatory;
                                                    objArryQue["fedtom_questiontype"] = $scope.QuestionList[i].questiontype;
                                                    objArryQue["fedtom_sequence"] = $scope.QuestionList[i].sequence;
                                                    objArryQue["fedtom_question"] = $scope.QuestionList[i].questiontext;
                                                    Xrm.WebApi.createRecord("fedtom_tomaquestions", objArryQue).then(
                                                                        function success(result) {
                                                                            if (i === $scope.QuestionList.length) {
                                                                                var windowOptions = {
                                                                                    openInNewWindow: false
                                                                                };
                                                                                if (Xrm.Utility != null) {
                                                                                    Xrm.Utility.openEntityForm("fedtom_tomaquestionnaire", recordId, null, windowOptions);
                                                                                }
                                                                            }
                                                                        },
                                                                        function (error) {
                                                                        }
                                                                    );
                                                }
                                            },
                                            function (errorTOMA) {
                                            }
                                        );
                        }
                        else {
                            var objArry = {};
                            objArry["fedtom_name"] = $scope.SurveyName;
                            objArry["fedtom_description"] = $scope.Description;
                            objArry["fedtom_isactive"] = $scope.IsActive;
                            if ($scope.SurveyName === "") {
                                Xrm.Page.ui.setFormNotification("Survey Name name is required.", "WARNING", "1")
                                setTimeout(function () {
                                    Xrm.Page.ui.clearFormNotification("1");
                                }, 3000);
                                return;
                            }
                            Xrm.WebApi.updateRecord("fedtom_tomaquestionnaire", TOMQID, objArry).then(
                                           function successTOMA(resultTOMA) {
                                               for (var i = 0; i < $scope.QuestionList.length; i++) {
                                                   if ($scope.QuestionList[i].queid === "") {
                                                       var objArryQue = {};
                                                       objArryQue["fedtom_Survey@odata.bind"] = "/fedtom_tomaquestionnaires(" + TOMQID + ")";
                                                       //objArryQue["fedtom_name"] = $scope.QuestionList[i].index;
                                                       objArryQue["fedtom_option1"] = $scope.QuestionList[i].option1txt;
                                                       objArryQue["fedtom_option2"] = $scope.QuestionList[i].option2txt;
                                                       objArryQue["fedtom_option3"] = $scope.QuestionList[i].option3txt;
                                                       objArryQue["fedtom_option4"] = $scope.QuestionList[i].option4txt;
                                                       objArryQue["fedtom_option5"] = $scope.QuestionList[i].option5txt;
                                                       objArryQue["fedtom_option6"] = $scope.QuestionList[i].option6txt;
                                                       objArryQue["fedtom_option7"] = $scope.QuestionList[i].option7txt;
                                                       objArryQue["fedtom_option8"] = $scope.QuestionList[i].option8txt;
                                                       objArryQue["fedtom_option9"] = $scope.QuestionList[i].option9txt;
                                                       objArryQue["fedtom_option10"] = $scope.QuestionList[i].option10txt;

                                                       objArryQue["fedtom_showoption1"] = $scope.QuestionList[i].option1show;
                                                       objArryQue["fedtom_showoption2"] = $scope.QuestionList[i].option2show;
                                                       objArryQue["fedtom_showoption3"] = $scope.QuestionList[i].option3show;
                                                       objArryQue["fedtom_showoption4"] = $scope.QuestionList[i].option4show;
                                                       objArryQue["fedtom_showoption5"] = $scope.QuestionList[i].option5show;
                                                       objArryQue["fedtom_showoption6"] = $scope.QuestionList[i].option6show;
                                                       objArryQue["fedtom_showoption7"] = $scope.QuestionList[i].option7show;
                                                       objArryQue["fedtom_showoption8"] = $scope.QuestionList[i].option8show;
                                                       objArryQue["fedtom_showoption9"] = $scope.QuestionList[i].option9show;
                                                       objArryQue["fedtom_showoption10"] = $scope.QuestionList[i].option10show;

                                                       objArryQue["fedtom_answer"] = $scope.QuestionList[i].answers;
                                                       objArryQue["fedtom_descriptiveanswer"] = $scope.QuestionList[i].descriptiveanswer;
                                                       objArryQue["fedtom_ismandatory"] = $scope.QuestionList[i].ismandatory;
                                                       objArryQue["fedtom_questiontype"] = $scope.QuestionList[i].questiontype;
                                                       objArryQue["fedtom_sequence"] = $scope.QuestionList[i].sequence;
                                                       objArryQue["fedtom_question"] = $scope.QuestionList[i].questiontext;
                                                       Xrm.WebApi.createRecord("fedtom_tomaquestions", objArryQue).then(
                                                                           function success(result) {
                                                                               if (i === $scope.QuestionList.length) {
                                                                                   var windowOptions = {
                                                                                       openInNewWindow: false
                                                                                   };
                                                                                   if (Xrm.Utility != null) {
                                                                                       Xrm.Utility.openEntityForm("fedtom_tomaquestionnaire", TOMQID, null, windowOptions);
                                                                                   }
                                                                               }
                                                                           },
                                                                           function (error) {
                                                                           }
                                                                       );
                                                   }
                                                   else {
                                                       var objArryQue = {};
                                                       objArryQue["fedtom_Survey@odata.bind"] = "/fedtom_tomaquestionnaires(" + TOMQID + ")";
                                                       //objArryQue["fedtom_name"] = $scope.QuestionList[i].index;
                                                       objArryQue["fedtom_option1"] = $scope.QuestionList[i].option1txt;
                                                       objArryQue["fedtom_option2"] = $scope.QuestionList[i].option2txt;
                                                       objArryQue["fedtom_option3"] = $scope.QuestionList[i].option3txt;
                                                       objArryQue["fedtom_option4"] = $scope.QuestionList[i].option4txt;
                                                       objArryQue["fedtom_option5"] = $scope.QuestionList[i].option5txt;
                                                       objArryQue["fedtom_option6"] = $scope.QuestionList[i].option6txt;
                                                       objArryQue["fedtom_option7"] = $scope.QuestionList[i].option7txt;
                                                       objArryQue["fedtom_option8"] = $scope.QuestionList[i].option8txt;
                                                       objArryQue["fedtom_option9"] = $scope.QuestionList[i].option9txt;
                                                       objArryQue["fedtom_option10"] = $scope.QuestionList[i].option10txt;

                                                       objArryQue["fedtom_showoption1"] = $scope.QuestionList[i].option1show;
                                                       objArryQue["fedtom_showoption2"] = $scope.QuestionList[i].option2show;
                                                       objArryQue["fedtom_showoption3"] = $scope.QuestionList[i].option3show;
                                                       objArryQue["fedtom_showoption4"] = $scope.QuestionList[i].option4show;
                                                       objArryQue["fedtom_showoption5"] = $scope.QuestionList[i].option5show;
                                                       objArryQue["fedtom_showoption6"] = $scope.QuestionList[i].option6show;
                                                       objArryQue["fedtom_showoption7"] = $scope.QuestionList[i].option7show;
                                                       objArryQue["fedtom_showoption8"] = $scope.QuestionList[i].option8show;
                                                       objArryQue["fedtom_showoption9"] = $scope.QuestionList[i].option9show;
                                                       objArryQue["fedtom_showoption10"] = $scope.QuestionList[i].option10show;

                                                       objArryQue["fedtom_answer"] = $scope.QuestionList[i].answers;
                                                       objArryQue["fedtom_descriptiveanswer"] = $scope.QuestionList[i].descriptiveanswer;
                                                       objArryQue["fedtom_ismandatory"] = $scope.QuestionList[i].ismandatory;
                                                       objArryQue["fedtom_questiontype"] = $scope.QuestionList[i].questiontype;
                                                       objArryQue["fedtom_sequence"] = $scope.QuestionList[i].sequence;
                                                       objArryQue["fedtom_question"] = $scope.QuestionList[i].questiontext;
                                                       Xrm.WebApi.updateRecord("fedtom_tomaquestions", $scope.QuestionList[i].queid, objArryQue).then(
                                                                           function success(result) {
                                                                               if (i === $scope.QuestionList.length) {
                                                                                   var windowOptions = {
                                                                                       openInNewWindow: false
                                                                                   };
                                                                                   if (Xrm.Utility != null) {
                                                                                       Xrm.Utility.openEntityForm("fedtom_tomaquestionnaire", TOMQID, null, windowOptions);
                                                                                   }
                                                                               }
                                                                           },
                                                                           function (error) {
                                                                           }
                                                                       );
                                                   }
                                               }
                                           },
                                           function (errorTOMA) {
                                           }
                                       );
                        }
                    }
                } catch (e) {
                }
            }
            $scope.AddQuestion = function () {
                try {
                    $scope.QuestionList.push(
                                   {
                                       "index": $scope.QuestionList.length + 1, "collapsecls": "panel-collapse collapse in",
                                       "questionname": "Question", "questiontext": "",
                                       "option1": "Option 1", "option2": "Option 2",
                                       "option3": "Option 3", "option4": "Option 4",
                                       "option5": "Option 5", "option6": "Option 6",
                                       "option7": "Option 7", "option8": "Option 8",
                                       "option9": "Option 9", "option10": "Option 10",
                                       "option1show": true, "option2show": true,
                                       "option3show": false, "option4show": false,
                                       "option5show": false, "option6show": false,
                                       "option7show": false, "option8show": false,
                                       "option9show": false, "option10show": false,
                                       "option1txt": "", "option2txt": "",
                                       "option3txt": "", "option4txt": "",
                                       "option5txt": "", "option6txt": "",
                                       "option7txt": "", "option8txt": "",
                                       "option9txt": "", "option10txt": "",
                                       "answers": "", "descriptiveanswer": "",
                                       "ismandatory": false, "questiontype": "0",
                                       "surveyid": "", "survey": "",
                                       "sequence": $scope.QuestionList.length + 1, "queid": "",
                                       "delete": true, "addoption": true,"questiontypename":""
                                   });
                } catch (e) {
                }
            }
            $scope.QuestionTypeChanged = function (question) {
                try {
                    if (question.questiontype === "3" || question.questiontype === 3) {
                        question.option1show = false;
                        question.option2show = false;
                        question.option3show = false;
                        question.option4show = false;
                        question.option5show = false;
                        question.option6show = false;
                        question.option7show = false;
                        question.option8show = false;
                        question.option9show = false;
                        question.option10show = false;
                        question.addoption = false;
                    }
                    else if (question.questiontype === "4" || question.questiontype === 4) {
                        question.option1show = false;
                        question.option2show = false;
                        question.option3show = false;
                        question.option4show = false;
                        question.option5show = false;
                        question.option6show = false;
                        question.option7show = false;
                        question.option8show = false;
                        question.option9show = false;
                        question.option10show = false;
                        question.addoption = false;
                    }
                    else  {
                        question.option1show = true;
                        question.option2show = true;
                        question.option3show = false;
                        question.option4show = false;
                        question.option5show = false;
                        question.option6show = false;
                        question.option7show = false;
                        question.option8show = false;
                        question.option9show = false;
                        question.option10show = false;
                        question.addoption = true;
                    }
                } catch (e) {

                }
            }
            $scope.AddOption = function (question) {
                try {
                    if (question.option3show === false) {
                        question.option3show = true;
                        return;
                    }
                    if (question.option4show === false) {
                        question.option4show = true;
                        return;
                    }
                    if (question.option5show === false) {
                        question.option5show = true;
                        return;
                    }
                    if (question.option6show === false) {
                        question.option6show = true;
                        return;
                    }
                    if (question.option7show === false) {
                        question.option7show = true;
                        return;
                    }
                    if (question.option8show === false) {
                        question.option8show = true;
                        return;
                    }
                    if (question.option9show === false) {
                        question.option9show = true;
                        return;
                    }
                    if (question.option10show === false) {
                        question.option10show = true;
                        return;
                    }
                } catch (e) {

                }
            }
            $scope.AddTemplate = function () {
                try {
                    var arr = [];
                    var saveCount = 0;
                    for (var i = 0; i < $scope.TemplateList.length; i++) {
                        if ($scope.TemplateList[i].isChecked) {
                            arr.push(
                                {
                                    "index": $scope.TemplateList[i].index,
                                    "templateid": $scope.TemplateList[i].templateid,
                                    "title": $scope.TemplateList[i].title,
                                    "description": $scope.TemplateList[i].description,
                                }
                                )
                        }
                    }
                    for (var i = 0; i < arr.length; i++) {
                        var objArry = {};
                        objArry["fedtom_TeamingRequestQuestionnaire@odata.bind"] = "/fedtom_tomaquestionnaires(" + TOMQID + ")";
                        objArry.fedtom_toma_email_template = arr[i].title;
                        objArry.fedtom_toma_email_templateid = arr[i].templateid;
                        objArry.fedtom_description = arr[i].description;
                        Xrm.WebApi.createRecord("fedtom_toma_questionaries_email_template", objArry).then(
                                            function success(result) {
                                                saveCount = saveCount + 1;
                                                if (saveCount === arr.length) {
                                                    GetTOMAEmailTemplate();
                                                    $('#myModal').modal('hide');
                                                }
                                            },
                                            function (error) {
                                            }
                                        );
                    }
                } catch (e) {
                }
            }
            // This executes when entity in table is checked
            $scope.allTemplatesSelected = false;
            $scope.selectTemplate = function () {
                // If any entity is not checked, then uncheck the "allItemsSelected" checkbox
                for (var i = 0; i < $scope.TemplateList.length; i++) {
                    if (!$scope.TemplateList[i].isChecked) {
                        $scope.allTemplatesSelected = false;
                        return;
                    }
                }

                //If not the check the "allItemsSelected" checkbox
                $scope.allTemplatesSelected = true;
            };

            // This executes when checkbox in table header is checked
            $scope.selectAllTemplate = function () {
                // Loop through all the entities and set their isChecked property
                for (var i = 0; i < $scope.TemplateList.length; i++) {
                    $scope.TemplateList[i].isChecked = $scope.allTemplatesSelected;
                }
            };
            $scope.Delete = function (emailTemplate) {
                try {
                    if (confirm("Are you sure you want to delete record?")) {
                        Xrm.WebApi.deleteRecord("fedtom_toma_questionaries_email_template", emailTemplate.guid).then(
                                            function success(result) {
                                                var index = $scope.TOMAEmailTemplateList.indexOf(emailTemplate);
                                                $scope.TOMAEmailTemplateList.splice(index, 1);
                                            },
                                            function (error) {
                                            }
                                        );
                        var index = $scope.TOMAEmailTemplateList.indexOf(emailTemplate);
                        $scope.TOMAEmailTemplateList.splice(index, 1);
                    }
                } catch (e) {

                }
            }
            $scope.DeleteQuestionnaire = function () {
                try {
                    if (confirm("Are you sure you want to delete record?")) {
                        Xrm.WebApi.deleteRecord("fedtom_tomaquestionnaire", TOMQID).then(
                                            function success(result) {
                                                //var windowOptions = {
                                                //    openInNewWindow: false
                                                //};
                                                //if (Xrm.Utility != null) {
                                                //    TOMQID = null;
                                                //    $scope.QuestionList = [];
                                                //    $scope.TemplateList = [];
                                                //    $scope.TOMAEmailTemplateList = [];
                                                //    $scope.SurveyName = "";
                                                //    $scope.Description = "";
                                                //    $scope.IsActive = false;
                                                //    $scope.dvView = false;
                                                //    if (TOMQID === "") {
                                                //        $scope.dvView = true;
                                                //    }
                                                //    GetQuestionnaires();
                                                //    GetTOMAEmailTemplate();
                                                //    Xrm.Utility.openEntityForm("fedtom_tomaquestionnaire", windowOptions);
                                                //}
                                               
                                                debugger;
                                                var parameters = {};
                                                Xrm.Utility.openEntityForm("fedtom_tomaquestionnaire", null, parameters, null);
                                            },
                                            function (error) {
                                            }
                                        );
                    }
                } catch (e) {
                }
            }
            $scope.DeleteQuestion = function (question) {
                try {
                    var dialogLabelAndText = { confirmButtonLabel: "Yes", cancelButtonLabel: "No", text: "Are you sure you want to delete question?", title: "Confirmation" };
                    var dialogOptions = { height: 200, width: 450 };
                    Xrm.Navigation.openConfirmDialog(dialogLabelAndText, dialogOptions).then(
                      function (success) {
                          if (success.confirmed) {
                              Xrm.WebApi.deleteRecord("fedtom_tomaquestions", question.queid).then(
                                                                         function success(result) {
                                                                             var windowOptions = {
                                                                                 openInNewWindow: false
                                                                             };
                                                                             if (Xrm.Utility != null) {
                                                                                 Xrm.Utility.openEntityForm("fedtom_tomaquestionnaire", TOMQID, null, windowOptions);
                                                                             }
                                                                         },
                                                                         function (error) {
                                                                         }
                                                                     );
                          }
                      });
                } catch (e) {
                }
            }
            $scope.Refresh = function () {
                try {
                    var windowOptions = {
                        openInNewWindow: false
                    };
                    if (Xrm.Utility != null) {
                        Xrm.Utility.openEntityForm("fedtom_tomaquestionnaire", TOMQID, null, windowOptions);
                    }
                } catch (e) {
                }
            }
        } catch (e) {
        }
    }
})(document, angular);