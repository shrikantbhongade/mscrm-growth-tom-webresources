﻿(function (document, angular) {
    "use strict";
    var app = angular.module('MyApp', []);//ngInputModified
    console.log("Into Vehicle Partner");
    app.controller('TeamingPartner', TeamingPartner);
    function TeamingPartner($scope) {
        try {
            var Xrm = parent.Xrm;
            var TOguid = Xrm.Page.data.entity.getId();
            var TOID = TOguid.substring(1, 37);
            //UserId = Xrm.Page.context.getUserId();

            console.log("TOID:- " + TOID);
            GetTeamingPartners();
            
            $scope.TeamingPartnersList = [];
            $scope.VehiclePartnersList = [];
            $scope.TaskOrderContactList = [];
            $scope.TOMAQuestionnaireList = [];
            $scope.TOMAEmailTemplateList = [];
            $scope.AttachmentList = [];
            $scope.allContactsSelected = false;
            $scope.AttachmentsSelected = false;
            $scope.displayTD = true;
            $scope.selectedServey = null;
            $scope.selectedTemplateDetails = null;
            $scope.rolelist = [{
                id: 918990000,
                value: "New"
            },
            {
                id: 918990001,
                value: "Request Sent"
            },
            {
            id: 918990002,
            value: "In Review"
            }
            ,
            {
                id: 918990003,
                value: "Selected"
            }
            ,
            {
                id: 918990004,
                value: "Not Selected"
            }
            ,
            {
                id: 918990005,
                value: "Partner Not Interested"
            }
            ];
            function GetTeamingPartners() {
                $scope.TeamingPartnersList = [];
                $scope.EditArray = [];
                $scope.savebtnCssClass = "slds-hide";
                try {
                    var qry = "?$select=fedcap_opportunityteamname,_fedcap_vehiclepartner_value,fedcap_partner,fedcap_partnerrole,fedcap_statusteamingpartners&$filter=_fedcap_taskorder_value eq " + TOID + "";
                    Xrm.WebApi.retrieveMultipleRecords("fedcap_teamingpartners", qry)
                       .then(
                       function success(responseTP) {
                           //$scope.TeamingPartnersList = responseTP.entities;
                           $scope.TeamingPartnersList = [];
                           for (var i = 0; i < responseTP.entities.length; i++) {
                               var roleid = "";//responseTP.entities[i]["new_tm_toma__role__c"];
                               var role = "";//responseTP.entities[i]["new_tm_toma__role__c@OData.Community.Display.V1.FormattedValue"];
                               if (roleid === null) {
                                   roleid = "";
                                   role = "";
                               }
                               $scope.TeamingPartnersList.push(
                                   {
                                       "index": i,
                                       "editclass": "slds-show",
                                       "undoclass": "slds-hide",
                                       "addclass": "slds-hide",
                                       "opportunityteamname":  responseTP.entities[i]["fedcap_opportunityteamname"],
                                       "teamingpartnersid": responseTP.entities[i]["fedcap_teamingpartnersid"],
                                       "vehiclepartnerid": responseTP.entities[i]["_fedcap_vehiclepartner_value"],
                                       "vehiclepartner": responseTP.entities[i]["_fedcap_vehiclepartner_value@OData.Community.Display.V1.FormattedValue"],
                                       "partner": responseTP.entities[i]["fedcap_partner"],
                                       "fedcap_statusteamingpartners": responseTP.entities[i]["fedcap_statusteamingpartners"],
                                       "statusid": responseTP.entities[i]["fedcap_statusteamingpartners"],
                                       "status": responseTP.entities[i]["fedcap_statusteamingpartners@OData.Community.Display.V1.FormattedValue"],
                                       "role": responseTP.entities[i]["fedcap_partnerrole"],
                                   });
                           }
                           GetVehiclePartners();
                           $scope.$apply();
                       },
                       function fail(error) {
                           var data = error;
                       }
                       );

                } catch (e) {
                }
            }
            function GetVehiclePartners() {
                debugger;
                var CVguid = Xrm.Page.getAttribute('fedcap_contractvehiclename').getValue()[0].id;
                var CVID = CVguid.substring(1, 37);
                $scope.VehiclePartnersList = [];
                try {
                    var qry = "?$select=new_name,_fedcap_accountforvehiclepartner_value,new_tm_toma__role__c,_new_tm_toma__contract_vehicle__c_value&$filter=_new_tm_toma__contract_vehicle__c_value eq " + CVID + "";
                    debugger;
                    Xrm.WebApi.retrieveMultipleRecords("new_vehiclepartners", qry)
                       .then(
                       function success(responseCV) {
                           debugger;
                           //var data = responseCV;
                           //$scope.VehiclePartnersList = responseCV.entities;
                           var VPCount = 0;
                           for (var i = 0; i < responseCV.entities.length; i++) {
                               debugger;
                               var filteredData = $scope.TeamingPartnersList.filter(
                                        function (response) {
                                            debugger;
                                            var vehiclepartnerid = response["vehiclepartnerid"];
                                            var new_vehiclepartnersid = responseCV.entities[i]["new_vehiclepartnersid"]
                                            return vehiclepartnerid === responseCV.entities[i]["new_vehiclepartnersid"];
                                        });
                               debugger;
                               if (filteredData.length === 0) {
                                   var roleid = responseCV.entities[i]["new_tm_toma__role__c"];
                                   var role = responseCV.entities[i]["new_tm_toma__role__c@OData.Community.Display.V1.FormattedValue"];
                                   if (roleid === null) {
                                       roleid = "";
                                       role = "";
                                   }
                                   var accid = responseCV.entities[i]["_fedcap_accountforvehiclepartner_value"];
                                   var acc = responseCV.entities[i]["_fedcap_accountforvehiclepartner_value@OData.Community.Display.V1.FormattedValue"];
                                   if (accid === null) {
                                       accid = "";
                                       acc = "";
                                   }
                                   $scope.VehiclePartnersList.push(
                                       {
                                           "index": VPCount,
                                           "vehiclepartnersid": responseCV.entities[i]["new_vehiclepartnersid"],
                                           "accountid": accid,
                                           "accountname": acc,
                                           "roleid": roleid,
                                           "role": role,
                                       });
                                   VPCount = VPCount + 1;
                               }
                               
                           }
                           $scope.$apply();
                       },
                       function fail(error) {
                           var data = error;
                       }
                       );

                } catch (e) {
                }
            }

            $scope.savebtnCssClass = "slds-hide";
            $scope.btnCssClass = "slds-show";
            $scope.EditArray = [];
            $scope.Edit = function (vp) {
                try {
                    debugger;
                    $scope.savebtnCssClass = "slds-show";
                    $scope.btnCssClass = "slds-hide";
                    vp.editclass = 'slds-hide';
                    vp.undoclass = 'slds-show';
                    $scope.EditArray.push(
                        {
                            "index": vp.index,
                            "editclass": vp.editclass,
                            "undoclass": vp.undoclass,
                            "addclass": vp.addclass,
                            "opportunityteamname": vp.opportunityteamname,
                            "teamingpartnersid": vp.teamingpartnersid,
                            "vehiclepartnerid": vp.vehiclepartnerid,
                            "vehiclepartner": vp.vehiclepartner,
                            "partner": vp.partner,
                            "fedcap_statusteamingpartners": vp.fedcap_statusteamingpartners,
                            "statusid": vp.statusid,
                            "status": vp.status,
                            "role": vp.role,
                        });
                    $scope.SaveBtnCount = $scope.SaveBtnCount + 1;

                } catch (e) {
                    debugger;
                }
                //$scope.TeamingPartnersList.forEach(function (v) {
                //    if (v.index === index) {
                //    }
                //});
            }
            $scope.Undo = function (vp) {
                debugger;
                try {
                    vp.editclass = 'slds-show';
                    vp.undoclass = 'slds-hide';
                    if ($scope.SaveBtnCount !== 0) {
                        $scope.SaveBtnCount = $scope.SaveBtnCount - 1;
                    }
                    if ($scope.SaveBtnCount === 0) {
                        $scope.savebtnCssClass = "slds-hide";
                        $scope.btnCssClass = "slds-show";
                        
                    }
                    var index = $scope.EditArray.indexOf(vp);
                    vp.statusid = vp.fedcap_statusteamingpartners;
                    $scope.EditArray.splice(index, 1);
                } catch (e) {

                }
            }
            $scope.Delete = function (vp) {
                try {
                    var dialogLabelAndText = { confirmButtonLabel: "Yes", cancelButtonLabel: "No", text: "Are you sure you want to delete this Teaming Partner record?", title: "Confirmation" };
                    var dialogOptions = { height: 200, width: 450 };
                    Xrm.Navigation.openConfirmDialog(dialogLabelAndText, dialogOptions).then(
                      function (success) {
                          if (success.confirmed) {
                              // todo code for deletion
                              if ($scope.SaveBtnCount !== 0) {
                                  $scope.SaveBtnCount = $scope.SaveBtnCount - 1;
                              }
                              if ($scope.SaveBtnCount === 0) {
                                  $scope.savebtnCssClass = "slds-hide";
                                  $scope.btnCssClass = "slds-show";

                              }
                              Xrm.WebApi.deleteRecord("fedcap_teamingpartners", vp.teamingpartnersid).then(
                                                  function success(result) {
                                                      var index = $scope.TeamingPartnersList.indexOf(vp);
                                                      $scope.TeamingPartnersList.splice(index, 1);
                                                      GetTeamingPartners();
                                                      Xrm.Navigation.openAlertDialog({ confirmButtonLabel: "Yes", text: "Record deleted!" });
                                                  },
                                                  function (error) {
                                                  }
                                              );
                          }
                      });
                   
                } catch (e) {
                }
            }
            $scope.SaveBtnCount = 0;
            $scope.SaveRecords = function () {
                try {
                    debugger;
                    $scope.SaveBtnCount = 0;
                    $scope.savebtnCssClass = "slds-hide";
                    $scope.btnCssClass = "slds-show";
                    for (var i = 0; i < $scope.EditArray.length; i++) {
                        var objArry = {};
                        var filteredData = $scope.TeamingPartnersList.filter(
                                       function (response) {
                                           var tp_id = response["teamingpartnersid"];
                                           return tp_id === $scope.EditArray[i]["teamingpartnersid"];
                                       });
                        debugger;
                        if (filteredData !== 0) {
                            objArry.fedcap_statusteamingpartners = filteredData[0].statusid;
                            Xrm.WebApi.updateRecord("fedcap_teamingpartners", filteredData[0].teamingpartnersid, objArry).then(
                                                function success(result) {
                                                    debugger;
                                                    var editLen = $scope.EditArray.length;
                                                    if (i === editLen) {
                                                        GetTeamingPartners();
                                                    }
                                                },
                                                function (error) {
                                                    debugger;
                                                }
                                            );
                        }
                        
                    }
                } catch (e) {
                    debugger;
                }
            }
            $scope.AddRow = function () {
                try {
                    for (var i = 0; i < $scope.VehiclePartnersList.length; i++) {
                        if ($("#chk_" + i + "").prop("checked") === true) {
                            var objArry = {};
                            debugger;
                            objArry["fedcap_VehiclePartner@odata.bind"] = "/fedcap_teamingpartners(" + vehiclepartnersid + ")";
                            //objArry.fedcap_statusteamingpartners = $scope.VehiclePartnersList[i].vehiclepartnersid;
                            Xrm.WebApi.createRecord("fedcap_teamingpartners", objArry).then(
                                                function success(result) {
                                                    debugger;
                                                    //if (i === $scope.EditArray.length - 1) {
                                                    //    GetTeamingPartners();
                                                    //}
                                                },
                                                function (error) {
                                                }
                                            );
                        }
                    }
                } catch (e) {
                }
            }
            $scope.AddTeamingRow = function () {
                debugger;
                try {
                    var arr = [];
                    for (var i = 0; i < $scope.VehiclePartnersList.length; i++) {
                        var chk = $("#chk_" + i + "").prop("checked");
                        if ($("#chk_" + i + "").prop("checked") === true) {
                            arr.push(
                                {
                                    "vehiclepartnersid": $scope.VehiclePartnersList[i].vehiclepartnersid
                                    ,"role": $scope.VehiclePartnersList[i].role
                                }
                                )
                        }
                    }
                    var saveCount = 0;
                    for (var i = 0; i < arr.length; i++) {
                        var objArry = {};
                        objArry["fedcap_VehiclePartner@odata.bind"] = "/fedcap_teamingpartnerses(" + arr[i].vehiclepartnersid + ")";
                        objArry["fedcap_TaskOrder@odata.bind"] = "/fedcap_teamingpartnerses(" + TOID + ")";
                        objArry.fedcap_statusteamingpartners = 918990000;
                        objArry.fedcap_partnerrole = arr[i].role;
                        
                        Xrm.WebApi.createRecord("fedcap_teamingpartners", objArry).then(
                                            function success(result) {
                                                debugger;
                                                saveCount = saveCount + 1;
                                                if (saveCount === arr.length) {
                                                    GetTeamingPartners();
                                                }
                                            },
                                            function (error) {
                                                debugger;
                                            }
                                        );
                    }
                } catch (e) {
                }
            }
            $scope.Refresh = function () {
                try {
                    $scope.SaveBtnCount = 0;
                    $scope.savebtnCssClass = "slds-hide";
                    $scope.btnCssClass = "slds-show";
                    GetTeamingPartners();
                } catch (e) {
                }
            }
            $scope.OpenOTP = function (cv) {
                try {
                    var parameters = {};
                    debugger;
                    var windowOptions = {
                        openInNewWindow: false
                    };
                    Xrm.Utility.openEntityForm("fedcap_teamingpartners", cv.teamingpartnersid, parameters, windowOptions);
                } catch (e) {
                }
            }
            $scope.OpenVP = function (cv) {
                try {
                    var parameters = {};
                    debugger;
                    var windowOptions = {
                        openInNewWindow: false
                    };
                    Xrm.Utility.openEntityForm("new_vehiclepartners", cv.vehiclepartnerid, parameters, windowOptions);
                } catch (e) {
                }
            }

            $scope.GetTaskOrderContacts = function () {
                $scope.TaskOrderContactList = [];
                $scope.allContactsSelected = false;
                try {
                    var qry = "?$select=fedcap_taskordercontact,_fedcap_accounts_value,_fedcap_taskorder_contact_value,fedcap_email,fedcap_mainpoc,fedcap_receivesmassemails&$filter=_fedcap_taskorder_value eq " + TOID + "";
                    Xrm.WebApi.retrieveMultipleRecords("fedcap_taskordercontacts", qry)
                       .then(
                       function success(responseCVC) {
                           for (var i = 0; i < responseCVC.entities.length; i++) {
                               var accid = responseCVC.entities[i]["_fedcap_accounts_value"];
                               var acc = responseCVC.entities[i]["_fedcap_accounts_value@OData.Community.Display.V1.FormattedValue"];
                               if (accid === null) {
                                   accid = "";
                                   acc = "";
                               }
                               var contactid = responseCVC.entities[i]["_fedcap_taskorder_contact_value"];
                               var contactname = responseCVC.entities[i]["_fedcap_taskorder_contact_value@OData.Community.Display.V1.FormattedValue"];
                               if (contactid === null) {
                                   contactid = "";
                                   contactname = "";
                               }
                               $scope.TaskOrderContactList.push(
                                   {
                                       "index": i,
                                       "editclass": "slds-show",
                                       "undoclass": "slds-hide",
                                       "addclass": "slds-hide",
                                       "tocid": responseCVC.entities[i]["fedcap_taskordercontactsid"],
                                       "accountid": accid,
                                       "accountname": acc,
                                       "contactid": contactid,
                                       "contactname": contactname,
                                       "email": responseCVC.entities[i]["fedcap_email"],
                                       "fedcap_mainpoc": responseCVC.entities[i]["fedcap_mainpoc"],
                                       "mainpoc": responseCVC.entities[i]["fedcap_mainpoc"],
                                       "fedcap_receivesmassemails": responseCVC.entities[i]["fedcap_receivesmassemails"],
                                       "receivesmassemails": responseCVC.entities[i]["fedcap_receivesmassemails"],
                                   });
                           }
                           $scope.$apply();
                           GetTOMAQuestionnaireList();
                           GetTOMAEmailTemplate("");
                           GetTaskOrdersAttachments();
                       },
                       function fail(error) {
                           var data = error;
                       }
                       );

                } catch (e) {
                }
            }
            function GetTOMAQuestionnaireList () {
                $scope.TOMAQuestionnaireList = [];
                try {
                    var qry = "?$select=fedtom_name,fedtom_tomaquestionnaireid";
                    Xrm.WebApi.retrieveMultipleRecords("fedtom_tomaquestionnaire", qry)
                       .then(
                       function success(responseTOMQue) {
                           $scope.TOMAQuestionnaireList = responseTOMQue.entities;
                           //for (var i = 0; i < responseTOMQue.entities.length; i++) {
                           //    $scope.TOMAQuestionnaireList.push(
                           //        {
                                       
                           //        });
                           //}
                           $scope.$apply();
                       },
                       function fail(error) {
                           var data = error;
                       }
                       );

                } catch (e) {
                }
            }
            function GetTOMAEmailTemplate(surveyId) {
                try {
                    debugger;
                    $scope.TOMAEmailTemplateList = [];
                    var qry = "";
                    if (surveyId === "") {
                        qry = "?$select=fedtom_toma_questionaries_email_templateid,fedtom_toma_email_templateid,fedtom_toma_email_template,fedtom_description,_fedtom_teamingrequestquestionnaire_value";
                    }
                    else {
                        qry = "?$select=fedtom_toma_questionaries_email_templateid,fedtom_toma_email_templateid,fedtom_toma_email_template,fedtom_description,_fedtom_teamingrequestquestionnaire_value&$filter=_fedtom_teamingrequestquestionnaire_value eq " + surveyId + "";
                    }
                    Xrm.WebApi.retrieveMultipleRecords("fedtom_toma_questionaries_email_template", qry)
                     .then(
                        function succ(response) {
                            debugger;
                            $scope.TOMAEmailTemplateList = response.entities;
                            
                            $scope.$apply();
                        },
                    function failed(error) {
                        var data = error;
                    }
                     );
                } catch (e) {

                }
            }
            function GetTaskOrdersAttachments() {
                try {
                    $scope.TOMAEmailTemplateList = [];
                    var qry = "?$select=filesize,filename,annotationid&$filter=_objectid_value eq " + TOID + "";
                    Xrm.WebApi.retrieveMultipleRecords("annotation", qry)
                     .then(
                        function succ(response) {
                            debugger;
                            $scope.AttachmentList = response.entities;
                            $scope.$apply();
                        },
                    function failed(error) {
                        debugger;
                        var data = error;
                    }
                     );
                } catch (e) {

                }
            }
            $scope.SelectedQuestionnarie = function (questionnaire) {
                try {
                    $scope.selectedServey = questionnaire;
                    GetTOMAEmailTemplate(questionnaire.fedtom_tomaquestionnaireid);
                } catch (e) {

                }
            };
            $scope.SelectedTemplate = function (template) {
                try {
                    $scope.selectedTemplateDetails = template;
                } catch (e) {

                }
            };
            $scope.NoQuestionnarie = function () {
                try {
                    $scope.displayTD = !$scope.displayTD;
                    $scope.selectedServey = null;
                    $scope.selectedTemplateDetails = null;
                    GetTOMAQuestionnaireList();
                    GetTOMAEmailTemplate("");
                } catch (e) {

                }
            };
            //************Email******************************
            $scope.SendMailCustom = function () {
                try {
                   
                    CreateEmailCustom();
                } catch (e) {

                }
            }
            $scope.txtSubject = '';
            $scope.txtCompose = '';
            function CreateEmailCustom() {
                debugger;
                var filteredContacts = $scope.TaskOrderContactList.filter(
                                                                         function (response) {
                                                                             var isChecked = response["isChecked"];
                                                                             return isChecked === true;
                                                                         });
                if (filteredContacts.length === 0) {
                    Xrm.Utility.alertDialog("Please select contact(s).");

                    return;
                }
                
                if ($scope.txtSubject === "") {
                    Xrm.Utility.alertDialog("Please enter Subject");
                    return;
                }
                if ($scope.txtCompose === "") {
                    Xrm.Utility.alertDialog("Please enter Description");
                    return;
                }
                var UserIdGuid = Xrm.Page.context.getUserId();
                var UserId = UserIdGuid.substring(1, 37);  //todo Email from User GUID
                var toPartyUsersId = ["609f2405-b8bc-e811-a966-000d3a308373"]; //todo Email To user array
                var webapiPath = Xrm.Page.context.getClientUrl() + "/api/data/v9.1/emails";
                var email = {};
                //Lookup
                //email["regardingobjectid_quote@odata.bind"] = "/quotes(" + quoteId + ")"; //Regarding is quote
                email["subject"] = $scope.txtSubject;
                email["description"] = $scope.txtCompose;
                var parties = [];
                //ActivityParty (From)
                var sender = {};
                sender["partyid_systemuser@odata.bind"] = "/systemusers(" + UserId + ")";
                sender["participationtypemask"] = 1; //From
                parties.push(sender);
                for (var i = 0; i < filteredContacts.length; i++) {
                    var receiver = {};
                    var contactId = filteredContacts[i].contactid;
                    receiver["partyid_contact@odata.bind"] = "/contacts(" + contactId + ")";
                    receiver["participationtypemask"] = 2; //To
                    parties.push(receiver);
                }
                //var receiver = {};
                //receiver["partyid_contact@odata.bind"] = "/contacts(609f2405-b8bc-e811-a966-000d3a308373)";
                //receiver["participationtypemask"] = 2; //To
                //parties.push(receiver);

                email["email_activity_parties"] = parties;
                var service = new XMLHttpRequest();
                service.open("POST", webapiPath, false);
                service.setRequestHeader("Accept", "application/json");
                service.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                service.setRequestHeader("OData-MaxVersion", "4.0");
                service.setRequestHeader("OData-Version", "4.0");
                service.send(JSON.stringify(email));
                if (service.readyState == 4) {
                    if (service.status == 204) {
                        debugger;
                        var uri = service.getResponseHeader("OData-EntityId");
                        var regExp = /\(([^)]+)\)/;
                        var matches = regExp.exec(uri);
                        var emailGblId = matches[1];
                        SendEmailCustom(emailGblId);
                    } else {

                        Xrm.Utility.alertDialog(this.statusText);
                    }
                }
            }

            function SendEmailCustom(emailGblId) {
                debugger;
                var parameters = {};
                parameters.IssueSend = true;
                var req = new XMLHttpRequest();
                req.open("POST", Xrm.Page.context.getClientUrl() + "/api/data/v8.1/emails(" + emailGblId + ")/Microsoft.Dynamics.CRM.SendEmail", true);
                req.setRequestHeader("OData-MaxVersion", "4.0");
                req.setRequestHeader("OData-Version", "4.0");
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.onreadystatechange = function () {
                    if (this.readyState === 4) {
                        req.onreadystatechange = null;
                        if (this.status === 200) {
                            debugger;
                            var results = JSON.parse(this.response);
                            Xrm.Utility.alertDialog("Email has been sent to selected contact(s). Thank you!");
                            $scope.txtSubject = '';
                            $scope.txtCompose = '';

                        } else {
                            Xrm.Utility.alertDialog(this.statusText);
                        }
                    }
                };
                req.send(JSON.stringify(parameters));
            }

            function successCallback() {
            }
            function errorCallback() {
            }
            $scope.SendMail = function () {
                try {
                    
                    var filteredContacts = $scope.TaskOrderContactList.filter(
                                      function (response) {
                                          var isChecked = response["isChecked"];
                                          return isChecked === true;
                                      });
                    if (filteredContacts.length === 0) {
                        Xrm.Utility.alertDialog("Please select contact(s).");
                        return;
                    }
                    if ($scope.selectedTemplateDetails === null) {
                        Xrm.Utility.alertDialog("Please select template.");
                        return;
                    }
                    var UserIdGuid = Xrm.Page.context.getUserId();
                    var UserId = UserIdGuid.substring(1, 37);
                    var notesIDs = "";
                    angular.forEach($scope.AttachmentList, function (notes, key) {
                        if (notes.isChecked) {
                            if (notesIDs === "") {
                                notesIDs = notes.annotationid;
                            }
                            else {
                                notesIDs += "," + notes.annotationid;
                            }
                        }
                    });
                    
                    var saveCount = 0;
                    angular.forEach($scope.TaskOrderContactList, function (contact, key) {
                        if (contact.isChecked) {
                            saveCount = saveCount + 1;
                            var objArry = {};
                            objArry["fedtom_Contact@odata.bind"] = "/contacts(" + contact.contactid + ")";
                            objArry["fedtom_TaskOrder@odata.bind"] = "/fedcap_taskorders(" + TOID + ")";
                            objArry["fedtom_TaskOrderContactLookup@odata.bind"] = "/fedcap_taskordercontactses(" + contact.tocid + ")";
                            objArry["fedtom_templatename"] = $scope.selectedTemplateDetails.fedtom_toma_email_template;
                            objArry["fedtom_notesid"] = notesIDs;
                            objArry["fedtom_subject"] = $scope.selectedServey.fedtom_name;
                            
                            debugger;
                            Xrm.WebApi.createRecord("fedtom_tomaquestionnairesent", objArry).then(
                                                function success(result) {
                                                    debugger;
                                                    if (saveCount === filteredContacts.length) {
                                                        //Xrm.Utility.alertDialog("Mail sent to seleted contacts!");
                                                        Xrm.Page.ui.setFormNotification("Mail sent to seleted contacts!", "INFO", "1")
                                                        setTimeout(function () {
                                                            Xrm.Page.ui.clearFormNotification("1");
                                                        }, 2000);
                                                        $('#myModal1').modal('hide');
                                                    }
                                                },
                                                function (error) {
                                                    debugger;
                                                }
                                            );
                        }
                    });
                    //angular.forEach($scope.TaskOrderContactList, function (contact, key) {
                    //    if (contact.isChecked) {
                    //        //var objArry = {};
                    //        //objArry["fedtom_Contact@odata.bind"] = "/contacts(" + contact.contactid + ")";
                    //        //objArry["fedtom_TaskOrder@odata.bind"] = "/fedcap_taskorders(" + TOID + ")";
                    //        //objArry["fedtom_templatename"] = $scope.selectedTemplateDetails.fedtom_toma_email_template;
                    //        //objArry["fedtom_notesid"] = notesIDs;
                    //        //objArry["fedtom_subject"] = $scope.selectedServey.fedtom_name;
                    //    }
                    //});
                    
                } catch (e) {

                }
            };

            // This executes when entity in table is checked
            $scope.selectContact = function () {
                // If any entity is not checked, then uncheck the "allItemsSelected" checkbox
                for (var i = 0; i < $scope.TaskOrderContactList.length; i++) {
                    if (!$scope.TaskOrderContactList[i].isChecked) {
                        $scope.allContactsSelected = false;
                        return;
                    }
                }

                //If not the check the "allItemsSelected" checkbox
                $scope.allContactsSelected = true;
            };
            // This executes when checkbox in table header is checked
            $scope.selectAllContact = function () {
                // Loop through all the entities and set their isChecked property
                for (var i = 0; i < $scope.TaskOrderContactList.length; i++) {
                    $scope.TaskOrderContactList[i].isChecked = $scope.allContactsSelected;
                }
            };

            $scope.selectAttachment = function () {
                // If any entity is not checked, then uncheck the "allItemsSelected" checkbox
                for (var i = 0; i < $scope.AttachmentList.length; i++) {
                    if (!$scope.AttachmentList[i].isChecked) {
                        $scope.AttachmentsSelected = false;
                        return;
                    }
                }

                //If not the check the "allItemsSelected" checkbox
                $scope.AttachmentsSelected = true;
            };
            // This executes when checkbox in table header is checked
            $scope.selectAllAttachment = function () {
                // Loop through all the entities and set their isChecked property
                for (var i = 0; i < $scope.AttachmentList.length; i++) {
                    $scope.AttachmentList[i].isChecked = $scope.AttachmentsSelected;
                }
            };
            $scope.OpenAccount = function (toc) {
                try {
                    var parameters = {};
                    debugger;
                    var windowOptions = {
                        openInNewWindow: false
                    };
                    Xrm.Utility.openEntityForm("account", toc.accountid, parameters, windowOptions);
                } catch (e) {

                }
            }
            $scope.OpenContact = function (toc) {
                try {
                    var parameters = {};
                    debugger;
                    var windowOptions = {
                        openInNewWindow: false
                    };
                    Xrm.Utility.openEntityForm("contact", toc.contactid, parameters, windowOptions);
                } catch (e) {

                }
            }
        } catch (e) {
        }
    }
})(document, angular);